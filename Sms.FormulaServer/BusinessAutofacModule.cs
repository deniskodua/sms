﻿namespace Sms.FormulaServer
{
    using System.Data.SqlClient;
    using Autofac;
    using Business.Compilation;
    using Business.Formula;
    using Business.Infrastructure;
    using Business.Runtime;
    using DataAccess;
    using DataAccess.Repositary;

    /// <summary>
    ///     Модуль для настройки зависимости между компонентами
    /// </summary>
    public class BusinessAutofacModule : Module
    {
        /// <summary>
        ///     Строка подключения к БД.
        /// </summary>
        private readonly string _smsDbConnectionString;

        /// <summary>
        ///     Конструктор класса.
        /// </summary>
        /// <param name="smsDbConnectionString">Строка подключения к БД.</param>
        public BusinessAutofacModule(string smsDbConnectionString)
        {
            this._smsDbConnectionString = smsDbConnectionString;
        }

        /// <inheritdoc />
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<FormulaRepositary>()
                .AsSelf()
                .InstancePerLifetimeScope();

            builder.RegisterType<FormulaProducer>()
                .As<IFormulaProducer>()
                .InstancePerLifetimeScope();

            builder.RegisterType<FormulaEngine>()
                .As<IFormulaEngine>()
                .SingleInstance()
                .OnActivated(_ => _.Instance.Initialize());

            builder.Register(c => new Config
                {
                    SmsDbConnectionString = this._smsDbConnectionString
                })
                .As<IConfig>()
                .SingleInstance();

            builder.RegisterType<SmsRuntime>()
                .As<ISmsRuntime>()
                .SingleInstance();

            builder.RegisterType<SmsCodeCompiler>()
                .As<ISmsCodeCompiler>()
                .SingleInstance();

            builder.Register(c => new SqlConnection(c.Resolve<IConfig>().SmsDbConnectionString))
                .Named<SqlConnection>("CodeDb")
                .InstancePerLifetimeScope();

            builder.Register(c => new CodeDbContext(c.ResolveNamed<SqlConnection>("CodeDb")))
                .AsSelf()
                .InstancePerLifetimeScope();
        }
    }
}