﻿namespace Sms.FormulaServer
{
    using System.Threading.Tasks;
    using Business.Formula;
    using Infrastructrure;
    using Models;

    /// <summary>
    /// Сервис расчета формул.
    /// </summary>
    [DefaultFault(typeof(DefaultErrorHandler))]
    public class FormulaService : IFormulaService
    {
        /// <summary>
        /// Поставшик формул.
        /// </summary>
        private readonly IFormulaProducer _formulaProducer;

        /// <summary>
        /// Конструктор класса.
        /// </summary>
        /// <param name="formulaProducer">Поставшик формул.</param>
        public FormulaService(IFormulaProducer formulaProducer)
        {
            this._formulaProducer = formulaProducer;
        }

        /// <inheritdoc/>
        public async Task AddFormula(string formula)
        {
            await this._formulaProducer.AddFormula(formula);
        }

        /// <inheritdoc/>
        public async Task<Variable> GetVariable(string name)
        {
            var variable = await this._formulaProducer.GetVariable(name);

            if (variable == null)
            {
                return null;
            }

            return new Variable
            {
                Value = variable.VariableValue,
                Name = variable.VariableName,
                ErrorDescription = variable.ErrorDescription,
                Status = variable.Status
            };
        }
    }
}