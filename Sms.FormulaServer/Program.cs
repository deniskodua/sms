﻿namespace Sms.FormulaServer
{
    using System;
    using System.Configuration;
    using System.ServiceModel;
    using Autofac;
    using Autofac.Integration.Wcf;

    internal class Program
    {
        private static void Main(string[] args)
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule(
                new BusinessAutofacModule(ConfigurationManager.ConnectionStrings["CodeDb"].ConnectionString));
            builder.RegisterModule(new FormulaServerAutofacModule());

            using (var container = builder.Build())
            {
                var host = new ServiceHost(typeof(FormulaService));
                host.AddDependencyInjectionBehavior<IFormulaService>(container);
                host.Open();
                Console.WriteLine("The host has been opened.");
                Console.ReadLine();

                host.Close();
                Environment.Exit(0);
            }
        }
    }
}