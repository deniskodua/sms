﻿namespace Sms.FormulaServer.Infrastructrure
{
    using System;
    using System.ServiceModel;
    using System.ServiceModel.Channels;
    using System.ServiceModel.Dispatcher;

    /// <summary>
    ///     Обработчик ошибок в WCF по умолчанию (оборачиваетвсе ошибки в FultException).
    /// </summary>
    public class DefaultErrorHandler : IErrorHandler
    {
        /// <inheritdoc />
        public bool HandleError(Exception error)
        {
            return true;
        }

        /// <inheritdoc />
        public void ProvideFault(Exception error,
            MessageVersion version,
            // ReSharper disable once RedundantAssignment
            ref Message fault)
        {
            var newEx = new FaultException(new FaultReason(error.Message));

            var msgFault = newEx.CreateMessageFault();
            fault = Message.CreateMessage(version, msgFault, newEx.Action);
        }
    }
}