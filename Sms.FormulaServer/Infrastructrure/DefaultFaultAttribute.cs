﻿namespace Sms.FormulaServer.Infrastructrure
{
    using System;
    using System.Collections.ObjectModel;
    using System.ServiceModel;
    using System.ServiceModel.Channels;
    using System.ServiceModel.Description;
    using System.ServiceModel.Dispatcher;

    /// <summary>
    ///     Атрибут для указания обработчика ошибток Wcf
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class DefaultFaultAttribute : Attribute, IServiceBehavior
    {
        /// <summary>
        ///     Тип обработчика ошибок
        /// </summary>
        private readonly Type _errorHandlerType;

        /// <summary>
        ///     Инициализирует новый экземпляр класса <see cref="DefaultFaultAttribute" />
        /// </summary>
        /// <param name="errorHandlerType">Тип обработчика ошибок</param>
        public DefaultFaultAttribute(Type errorHandlerType)
        {
            this._errorHandlerType = errorHandlerType;
        }

        /// <inheritdoc />
        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase,
            Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
        {
        }

        /// <inheritdoc />
        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            var errorHandler = (IErrorHandler) Activator.CreateInstance(this._errorHandlerType);

            foreach (var channelDispatcherBase in serviceHostBase.ChannelDispatchers)
            {
                var channelDispatcher = (ChannelDispatcher) channelDispatcherBase;
                channelDispatcher.ErrorHandlers.Add(errorHandler);
            }
        }

        /// <inheritdoc />
        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
        }
    }
}