﻿namespace Sms.FormulaServer
{
    using System.ServiceModel;
    using System.Threading.Tasks;
    using Models;

    /// <summary>
    /// Интерфейс сервиса по расчету формул.
    /// </summary>
    [ServiceContract]
    public interface IFormulaService
    {
        /// <summary>
        /// Добавление новой формулы.
        /// </summary>
        /// <param name="formula">Код формулы.</param>
        /// <returns>Задача добавления</returns>
        [OperationContract]
        Task AddFormula(string formula);

        /// <summary>
        /// Возвращает переменную формулы по имени.
        /// </summary>
        /// <param name="name">Имя переменной</param>
        /// <returns>Задача получения переменной формулы</returns>
        [OperationContract]
        Task<Variable> GetVariable(string name);
    }
}