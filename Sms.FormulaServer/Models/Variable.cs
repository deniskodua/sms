﻿namespace Sms.FormulaServer.Models
{
    using System.Runtime.Serialization;
    using DataAccess.Models;

    /// <summary>
    /// Модель данных для переменной в WCF.
    /// </summary>
    [DataContract]
    public class Variable
    {
        /// <summary>
        ///     Название переменной.
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        ///     Значение переменной.
        /// </summary>
        [DataMember]
        public double? Value { get; set; }

        /// <summary>
        ///     Описание ошибки.
        /// </summary>
        [DataMember]
        public string ErrorDescription { get; set; }

        /// <summary>
        ///     Статус.
        /// </summary>
        [DataMember]
        public FormulaStatus Status { get; set; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"{nameof(this.Name)}: {this.Name}, {nameof(this.Value)}: {this.Value}, {nameof(this.ErrorDescription)}: {this.ErrorDescription}, {nameof(this.Status)}: {this.Status}";
        }
    }
}