﻿namespace Sms.FormulaServer
{
    using Autofac;

    /// <summary>
    ///     Модуль для настройки зависимости между компонентами для проекта Sms.FormulaServer.
    /// </summary>
    public class FormulaServerAutofacModule : Module
    {
        /// <inheritdoc />
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<FormulaService>()
                .As<IFormulaService>();
        }
    }
}