﻿namespace Sms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Formula",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Status = c.Int(nullable: false),
                        CodeText = c.String(nullable: false),
                        VariableName = c.String(nullable: false, maxLength: 255),
                        VariableValue = c.Double(),
                        ErrorDescription = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Formula");
        }
    }
}
