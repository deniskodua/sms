﻿namespace Sms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class VariableNameCollation : DbMigration
    {
        public override void Up()
        {
            this.Sql("alter table dbo.Formula alter column CodeText nvarchar(max) collate Latin1_General_CS_AS not null");

            this.Sql("alter table dbo.Formula alter column VariableName nvarchar(255) collate Latin1_General_CS_AS not null");

            this.Sql("create unique index IX_Formula_VariableName on dbo.Formula(VariableName)");
        }

        public override void Down()
        {
            this.Sql("drop index dbo.Formula.IX_Formula_VariableName");

            this.Sql("alter table dbo.Formula alter column VariableName nvarchar(255) collate Latin1_General_CI_AS not null");

            this.Sql("alter table dbo.Formula alter column CodeText nvarchar(max) collate Latin1_General_CS_AS not null");
        }
    }
}
