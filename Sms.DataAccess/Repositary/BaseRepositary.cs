﻿namespace Sms.DataAccess.Repositary
{
    using System.Data.Entity;
    using System.Linq;
    using System.Threading.Tasks;
    using Models;

    /// <summary>
    ///     Базовый класс репозитария.
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public abstract class BaseRepositary<TEntity> : IRepositary<TEntity>
        where TEntity : class, IEntity
    {
        /// <summary>
        ///     Контекст БД.
        /// </summary>
        private readonly CodeDbContext _db;

        /// <summary>
        ///     Конструктор класса.
        /// </summary>
        /// <param name="db">Контекст БД.</param>
        protected BaseRepositary(CodeDbContext db)
        {
            this._db = db;
        }

        /// <inheritdoc />
        public virtual IQueryable<TEntity> Get()
        {
            return this._db.Set<TEntity>().AsNoTracking();
        }

        /// <inheritdoc />
        public virtual async Task<TEntity> GetById(int id)
        {
            return await this._db.Set<TEntity>()
                .AsNoTracking()
                .FirstOrDefaultAsync(e => e.Id == id);
        }

        /// <inheritdoc />
        public virtual async Task Create(TEntity entity)
        {
            this._db.Set<TEntity>().Add(entity);
            await this._db.SaveChangesAsync();
        }

        /// <inheritdoc />
        public virtual async Task Update(TEntity entity)
        {
            var loclEntity = this._db.Set<TEntity>().Local.FirstOrDefault(_ => _.Id == entity.Id);
            if (loclEntity != null)
            {
                this._db.Entry(loclEntity).State = EntityState.Detached;
            }

            this._db.Entry(entity).State = EntityState.Modified;
            await this._db.SaveChangesAsync();
        }

        /// <inheritdoc />
        public virtual async Task Delete(TEntity entity)
        {
            this._db.Set<TEntity>().Remove(entity);
            await this._db.SaveChangesAsync();
        }
    }
}