﻿namespace Sms.DataAccess.Repositary
{
    using System.Linq;
    using System.Threading.Tasks;
    using Models;

    /// <summary>
    ///     Интерфейс репозитария сущности БД.
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public interface IRepositary<TEntity>
        where TEntity : class, IEntity
    {
        /// <summary>
        ///     Получает коллекцию элементов.
        /// </summary>
        /// <returns>Коллекция элементов</returns>
        IQueryable<TEntity> Get();

        /// <summary>
        ///     Получает элемент по ИД.
        /// </summary>
        /// <param name="id">Ид элемента</param>
        /// <returns></returns>
        Task<TEntity> GetById(int id);

        /// <summary>
        ///     Создает элемент.
        /// </summary>
        /// <param name="entity">Создаваемый элемент</param>
        /// <returns>Задача обновления</returns>
        Task Create(TEntity entity);

        /// <summary>
        ///     Обновляет элемент.
        /// </summary>
        /// <param name="entity">Обновляемый элемент</param>
        /// <returns>Задача обновления</returns>
        Task Update(TEntity entity);

        /// <summary>
        ///     Удаляет элемент из БД
        /// </summary>
        /// <param name="entity">Удаляемый элемент</param>
        /// <returns>Задача обновления</returns>
        Task Delete(TEntity entity);
    }
}