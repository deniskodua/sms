﻿namespace Sms.DataAccess.Repositary
{
    using Models;

    /// <summary>
    ///     Репозитарий для формул.
    /// </summary>
    public class FormulaRepositary : BaseRepositary<FormulaEntity>
    {
        /// <summary>
        ///     Конструктор класса
        /// </summary>
        /// <param name="db">Контекст БД</param>
        public FormulaRepositary(CodeDbContext db) : base(db)
        {
        }
    }
}