﻿namespace Sms.DataAccess.Models
{
    /// <summary>
    ///     Интерфейс сущности БД.
    /// </summary>
    public interface IEntity
    {
        /// <summary>
        ///     Ид сущности.
        /// </summary>
        int Id { get; set; }
    }
}