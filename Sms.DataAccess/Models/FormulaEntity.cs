﻿// Copyright © 2018. i-Sys. All rights reserved.

namespace Sms.DataAccess.Models
{
    using System;
    using System.Data.Entity.ModelConfiguration;

    /// <summary>
    ///     Формула
    /// </summary>
    public class FormulaEntity : IEntity, ICloneable
    {
        /// <summary>
        ///     Статус формулы
        /// </summary>
        public FormulaStatus Status { get; set; }

        /// <summary>
        ///     Код формулы
        /// </summary>
        public string CodeText { get; set; }

        /// <summary>
        ///     Название переменной
        /// </summary>
        public string VariableName { get; set; }

        /// <summary>
        ///     Значение переменной.
        /// </summary>
        public double? VariableValue { get; set; }

        /// <summary>
        ///     Описание ошибки
        /// </summary>
        public string ErrorDescription { get; set; }

        /// <summary>
        /// Признак завершения расчета формулы.
        /// </summary>
        public bool IsTerminated => this.Status == FormulaStatus.Calculated || this.Status == FormulaStatus.Failed;

        /// <inheritdoc />
        public object Clone()
        {
            return this.MemberwiseClone();
        }

        /// <summary>
        ///     ИД записи.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        ///     Конфигуратор сущности БД
        /// </summary>
        internal class Configuration : EntityTypeConfiguration<FormulaEntity>
        {
            public Configuration()
            {
                this.ToTable("Formula");

                this.HasKey(_ => _.Id);

                this.Property(_ => _.Id)
                    .HasColumnOrder(0);

                this.Property(_ => _.Status)
                    .HasColumnOrder(1)
                    .IsRequired();

                this.Property(_ => _.CodeText)
                    .HasColumnOrder(2)
                    .HasColumnType("nvarchar(max)")
                    .IsRequired();

                this.Property(_ => _.VariableName)
                    .HasMaxLength(255)
                    .HasColumnOrder(3)
                    .IsRequired();

                this.Property(_ => _.VariableValue)
                    .HasColumnOrder(4);

                this.Property(_ => _.ErrorDescription)
                    .HasColumnOrder(5)
                    .HasColumnType("nvarchar(max)");
            }
        }
    }
}