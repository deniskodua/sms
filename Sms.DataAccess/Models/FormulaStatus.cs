﻿namespace Sms.DataAccess.Models
{
    /// <summary>
    ///     Статус формулы.
    /// </summary>
    public enum FormulaStatus
    {
        /// <summary>
        ///     Еще не вычислялась.
        /// </summary>
        NotCalculated = 0,

        /// <summary>
        ///     Ждет вычисления зависимой переменной.
        /// </summary>
        WaitingForDependency = 1,

        /// <summary>
        ///     Успешно вычислена.
        /// </summary>
        Calculated = 2,

        /// <summary>
        ///     Ошибка вычисления.
        /// </summary>
        Failed = 3
    }
}