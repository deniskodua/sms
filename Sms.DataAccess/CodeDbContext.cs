﻿namespace Sms.DataAccess
{
    using System.Data.Common;
    using System.Data.Entity;
    using Migrations;
    using Models;

    /// <summary>
    ///     Контекст базы данных.
    /// </summary>
    public class CodeDbContext : DbContext
    {
        /// <summary>
        ///     Конструктор класса.
        /// </summary>
        /// <param name="connection">Подключение к БД.</param>
        public CodeDbContext(DbConnection connection) : base(connection, false)
        {
            this.Configuration.ProxyCreationEnabled = false;
            this.Configuration.LazyLoadingEnabled = false;
        }

        /// <summary>
        ///     Конструктор класса.
        /// </summary>
        public CodeDbContext()
            : base("data source=.;initial catalog=SmsCodeDb;Integrated Security=True;App=SmsCodeCalculator")
        {
        }

        /// <summary>
        ///     Формулы.
        /// </summary>
        public virtual DbSet<FormulaEntity> Formulas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<CodeDbContext, Configuration>());
            modelBuilder.Configurations.Add(new FormulaEntity.Configuration());
        }
    }
}