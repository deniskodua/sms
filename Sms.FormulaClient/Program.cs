﻿namespace Sms.FormulaClient
{
    using System;
    using System.Globalization;
    using System.Linq;
    using Client;

    internal class Program
    {
        private static void Main(string[] args)
        {
            var client = new FormulaServiceClient();

            while (true)
            {
                Console.Write(">");
                var line = Console.ReadLine() ?? string.Empty;
                string formulaOrVariable = null;
                string command;
                var ix = line.IndexOf(' ');
                if (ix >= 0)
                {
                    formulaOrVariable = line.Substring(ix + 1);
                    command = line.Substring(0, ix);
                }
                else
                {
                    command = line;
                }

                switch (command)
                {
                    case "-f":
                        try
                        {
                            client.AddFormulaAsync(formulaOrVariable).Wait();
                            Console.WriteLine("Formula was successfully added");
                        }
                        catch (AggregateException e)
                        {
                            Console.WriteLine($"Error: {e.Flatten().InnerExceptions.First().Message}");
                        }

                        break;
                    case "-v":
                        try
                        {
                            var variable = client.GetVariableAsync(formulaOrVariable).Result;
                            if (variable == null)
                            {
                                Console.WriteLine($"Variable '{formulaOrVariable}' was not found on server");
                            }
                            else if (!string.IsNullOrEmpty(variable.ErrorDescription))
                            {
                                Console.WriteLine(variable.ErrorDescription);
                            }
                            else
                            {
                                Console.WriteLine(Convert.ToString(variable.Value, CultureInfo.CurrentUICulture));
                            }
                        }
                        catch (AggregateException e)
                        {
                            Console.WriteLine($"Error: {e.Flatten().InnerExceptions.First().Message}");
                        }

                        break;
                    case "-q":
                        Environment.Exit(0);
                        break;
                }
            }
        }
    }
}