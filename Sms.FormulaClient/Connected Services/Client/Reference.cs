﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sms.FormulaClient.Client {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Variable", Namespace="http://schemas.datacontract.org/2004/07/Sms.FormulaServer.Models")]
    [System.SerializableAttribute()]
    public partial class Variable : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ErrorDescriptionField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string NameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private Sms.FormulaClient.Client.FormulaStatus StatusField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Nullable<double> ValueField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ErrorDescription {
            get {
                return this.ErrorDescriptionField;
            }
            set {
                if ((object.ReferenceEquals(this.ErrorDescriptionField, value) != true)) {
                    this.ErrorDescriptionField = value;
                    this.RaisePropertyChanged("ErrorDescription");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name {
            get {
                return this.NameField;
            }
            set {
                if ((object.ReferenceEquals(this.NameField, value) != true)) {
                    this.NameField = value;
                    this.RaisePropertyChanged("Name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public Sms.FormulaClient.Client.FormulaStatus Status {
            get {
                return this.StatusField;
            }
            set {
                if ((this.StatusField.Equals(value) != true)) {
                    this.StatusField = value;
                    this.RaisePropertyChanged("Status");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Nullable<double> Value {
            get {
                return this.ValueField;
            }
            set {
                if ((this.ValueField.Equals(value) != true)) {
                    this.ValueField = value;
                    this.RaisePropertyChanged("Value");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="FormulaStatus", Namespace="http://schemas.datacontract.org/2004/07/Sms.DataAccess.Models")]
    public enum FormulaStatus : int {
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        NotCalculated = 0,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        WaitingForDependency = 1,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Calculated = 2,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Failed = 3,
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="Client.IFormulaService")]
    public interface IFormulaService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IFormulaService/AddFormula", ReplyAction="http://tempuri.org/IFormulaService/AddFormulaResponse")]
        void AddFormula(string formula);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IFormulaService/AddFormula", ReplyAction="http://tempuri.org/IFormulaService/AddFormulaResponse")]
        System.Threading.Tasks.Task AddFormulaAsync(string formula);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IFormulaService/GetVariable", ReplyAction="http://tempuri.org/IFormulaService/GetVariableResponse")]
        Sms.FormulaClient.Client.Variable GetVariable(string name);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IFormulaService/GetVariable", ReplyAction="http://tempuri.org/IFormulaService/GetVariableResponse")]
        System.Threading.Tasks.Task<Sms.FormulaClient.Client.Variable> GetVariableAsync(string name);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IFormulaServiceChannel : Sms.FormulaClient.Client.IFormulaService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class FormulaServiceClient : System.ServiceModel.ClientBase<Sms.FormulaClient.Client.IFormulaService>, Sms.FormulaClient.Client.IFormulaService {
        
        public FormulaServiceClient() {
        }
        
        public FormulaServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public FormulaServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public FormulaServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public FormulaServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public void AddFormula(string formula) {
            base.Channel.AddFormula(formula);
        }
        
        public System.Threading.Tasks.Task AddFormulaAsync(string formula) {
            return base.Channel.AddFormulaAsync(formula);
        }
        
        public Sms.FormulaClient.Client.Variable GetVariable(string name) {
            return base.Channel.GetVariable(name);
        }
        
        public System.Threading.Tasks.Task<Sms.FormulaClient.Client.Variable> GetVariableAsync(string name) {
            return base.Channel.GetVariableAsync(name);
        }
    }
}
