﻿namespace Sms.Business.Runtime
{
    using System.Collections.Generic;

    /// <summary>
    ///     Контекст выполнения кода виртуальной машиной.
    /// </summary>
    public class RunCodeContext
    {
        /// <summary>
        ///     Словарь переменных.
        /// </summary>
        public Dictionary<string, double> Variables { get; } = new Dictionary<string, double>();
    }
}