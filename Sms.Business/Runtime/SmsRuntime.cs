﻿namespace Sms.Business.Runtime
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;

    /// <summary>
    ///     Виртуальная машина для исполнения кода.
    /// </summary>
    public class SmsRuntime : ISmsRuntime
    {
        /// <inheritdoc />
        public virtual void RunCode(RunCodeContext context, string asmCode)
        {
            var stack = new Stack<double>();
            using (var sr = new StringReader(asmCode))
            {
                while (true)
                {
                    var line = sr.ReadLine();
                    if (line == null)
                    {
                        break;
                    }

                    double op1, op2;
                    string varOrConst;
                    var command = line.Split(new[] {' '}, 2, StringSplitOptions.RemoveEmptyEntries);
                    switch (command[0])
                    {
                        case "push":
                            varOrConst = command[1];
                            if (varOrConst.StartsWith("["))
                            {
                                varOrConst = varOrConst.TrimStart('[').TrimEnd(']');
                                stack.Push(context.Variables[varOrConst]);
                            }
                            else
                            {
                                stack.Push(Convert.ToDouble(varOrConst, CultureInfo.InvariantCulture));
                            }

                            break;
                        case "set":
                            op1 = stack.Pop();
                            varOrConst = command[1];
                            varOrConst = varOrConst.TrimStart('[').TrimEnd(']');
                            if (context.Variables.ContainsKey(varOrConst))
                            {
                                context.Variables[varOrConst] = op1;
                            }
                            else
                            {
                                context.Variables.Add(varOrConst, op1);
                            }

                            break;
                        case "add":
                            op2 = stack.Pop();
                            op1 = stack.Pop();
                            stack.Push(op1 + op2);
                            break;
                        case "sub":
                            op2 = stack.Pop();
                            op1 = stack.Pop();
                            stack.Push(op1 - op2);
                            break;
                        case "mul":
                            op2 = stack.Pop();
                            op1 = stack.Pop();
                            stack.Push(op1 * op2);
                            break;
                        case "div":
                            op2 = stack.Pop();
                            op1 = stack.Pop();
                            stack.Push(op1 / op2);
                            break;
                    }
                }
            }
        }
    }
}