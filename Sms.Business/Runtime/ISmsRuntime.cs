﻿namespace Sms.Business.Runtime
{
    /// <summary>
    ///     Интерфейс виртуальной машины
    /// </summary>
    public interface ISmsRuntime
    {
        /// <summary>
        ///     Исполняет скомпилированный код.
        /// </summary>
        /// <param name="context">Контекст выполнения виртуальной машины</param>
        /// <param name="asmCode">Скомпилированный код.</param>
        void RunCode(RunCodeContext context, string asmCode);
    }
}