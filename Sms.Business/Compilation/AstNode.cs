﻿namespace Sms.Business.Compilation
{
    /// <summary>
    ///     Узел абстрактного синтаксического дерева.
    /// </summary>
    public class AstNode
    {
        /// <summary>
        ///     Конструктор класса.
        /// </summary>
        /// <param name="token">Лексема</param>
        /// <param name="type">Тип узла</param>
        public AstNode(Lexeme token, AstNodeType type)
        {
            this.Token = token;
            this.Type = type;
            this.Children = new AstNodeList(this);
        }

        /// <summary>
        ///     Лексема
        /// </summary>
        public Lexeme Token { get; }

        /// <summary>
        ///     Тип узла.
        /// </summary>
        public AstNodeType Type { get; }

        /// <summary>
        ///     Родительский узел в дереве.
        /// </summary>
        public AstNode Parent { get; internal set; }

        /// <summary>
        ///     Дочерние узлы.
        /// </summary>
        public AstNodeList Children { get; }

        /// <summary>
        ///     Возвращает True, если данный узел является оператором языка, иначе - False.
        /// </summary>
        public bool IsOperator => this.Type == AstNodeType.Assign
                                  || this.Type == AstNodeType.Multiply
                                  || this.Type == AstNodeType.BinaryMinus
                                  || this.Type == AstNodeType.UnaryMinus
                                  || this.Type == AstNodeType.Divide
                                  || this.Type == AstNodeType.BinaryPlus
                                  || this.Type == AstNodeType.UnaryPlus;

        /// <summary>
        ///     Возвращает True, если данный узел является унарным оператором языка, иначе - False.
        /// </summary>
        public bool IsUnaryOperator => this.Type == AstNodeType.UnaryMinus
                                       || this.Type == AstNodeType.UnaryPlus;

        /// <summary>
        ///     Добавляет дочерний узел.
        /// </summary>
        /// <param name="child">Дочерний узел.</param>
        /// <returns>Текущий узел</returns>
        public AstNode AddChild(params AstNode[] child)
        {
            foreach (var c in child)
            {
                this.Children.AddLast(c);
            }

            return this;
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return $"{nameof(this.Token)}: {this.Token}, {nameof(this.Type)}: {this.Type}";
        }
    }
}