﻿namespace Sms.Business.Compilation
{
    /// <summary>
    ///     Интерфейс компонента-компилятора кода Sms.
    /// </summary>
    public interface ISmsCodeCompiler
    {
        /// <summary>
        ///     Выполняет лексический анализ кода.
        /// </summary>
        /// <param name="code">Анализируемый код.</param>
        /// <returns>Первая лексема кода.</returns>
        Lexeme DoLexicalAnalysis(string code);

        /// <summary>
        ///     Строит абстрактное синтаксическое дерево.
        ///     За основу взят алгоритм <see href="https://en.wikipedia.org/wiki/Shunting-yard_algorithm"/>
        /// </summary>
        /// <param name="lexeme">Первая лексема кода.</param>
        /// <returns>Абстрактное синтаксическое дерево.</returns>
        AstNode BuildAst(Lexeme lexeme);

        /// <summary>
        ///     Компилирует код в код ассемблера.
        /// </summary>
        /// <param name="code">Компилируемый код.</param>
        /// <returns>Ассемблерный код</returns>
        string Compile(string code);

        /// <summary>
        ///     Компилирует дерево Ast в код ассемблера.
        /// </summary>
        /// <param name="tree">Дерево Ast</param>
        /// <returns>Ассемблерный код</returns>
        string Compile(AstNode tree);
    }
}