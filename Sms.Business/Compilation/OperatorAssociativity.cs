﻿namespace Sms.Business.Compilation
{
    /// <summary>
    ///     Тип ассоциативности оператора.
    /// </summary>
    public enum OperatorAssociativity
    {
        /// <summary>
        ///     Левоассоциативный.
        /// </summary>
        Left,

        /// <summary>
        ///     Правоассоциативный.
        /// </summary>
        Right
    }
}