﻿namespace Sms.Business.Compilation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    ///     Компилятор Sms кода.
    /// </summary>
    public class SmsCodeCompiler : ISmsCodeCompiler
    {
        /// <inheritdoc />
        public Lexeme DoLexicalAnalysis(string code)
        {
            Lexeme current, first;
            current = first = new Lexeme(LexemeType.None, null, -1);

            int i = 0;
            while (i < code.Length)
            {
                var chr = code[i];

                if (char.IsWhiteSpace(chr))
                {
                    i++;
                    continue;
                }

                if (chr == '=')
                {
                    current.Next = new Lexeme(LexemeType.Assign, chr.ToString(), i)
                    {
                        Prev = current
                    };
                    i++;
                }
                else if (chr == '/')
                {
                    current.Next = new Lexeme(LexemeType.Divide, chr.ToString(), i)
                    {
                        Prev = current
                    };
                    i++;
                }
                else if (chr == '*')
                {
                    current.Next = new Lexeme(LexemeType.Multiply, chr.ToString(), i)
                    {
                        Prev = current
                    };
                    i++;
                }
                else if (chr == '+')
                {
                    current.Next = new Lexeme(LexemeType.Plus, chr.ToString(), i)
                    {
                        Prev = current
                    };
                    i++;
                }
                else if (chr == '-')
                {
                    current.Next = new Lexeme(LexemeType.Minus, chr.ToString(), i)
                    {
                        Prev = current
                    };
                    i++;
                }
                else if (chr == '(')
                {
                    current.Next = new Lexeme(LexemeType.LeftParen, chr.ToString(), i)
                    {
                        Prev = current
                    };
                    i++;
                }
                else if (chr == ')')
                {
                    current.Next = new Lexeme(LexemeType.RightParen, chr.ToString(), i)
                    {
                        Prev = current
                    };
                    i++;
                }
                else if (char.IsDigit(chr))
                {
                    var startIndex = i;
                    var len = this.GetIntNumberLength(code, i);
                    if (i + 1 < code.Length && code[i + 1] == '.')
                    {
                        var lenFractional = this.GetIntNumberLength(code, i + 2);
                        if (lenFractional > 0)
                        {
                            len += lenFractional + 1;
                        }
                    }

                    i += len;
                    current.Next = new Lexeme(LexemeType.Number, code.Substring(startIndex, len), startIndex)
                    {
                        Prev = current
                    };
                }
                else if (char.IsLetter(chr))
                {
                    var startIndex = i;
                    var len = this.GetIdentifierLength(code, i);
                    i += len;
                    current.Next = new Lexeme(LexemeType.Identifier, code.Substring(startIndex, len), startIndex)
                    {
                        Prev = current
                    };
                }
                else
                {
                    throw new SmsCodeCompileException($"Invalid token({chr}) at position {i}");
                }

                current = current.Next;
            }

            if (first.Next != null)
            {
                first.Next.Prev = null;
            }

            return first.Next;
        }

        /// <inheritdoc />
        public AstNode BuildAst(Lexeme lexeme)
        {
            var astStack = new Stack<AstNode>();
            var operatorStack = new Stack<AstNode>();
            for (; lexeme != null; lexeme = lexeme.Next)
            {
                if (lexeme.Type == LexemeType.Number)
                {
                    /*
                     * if the token is a number, then:
                        push it to the output queue.
                     */

                    astStack.Push(new AstNode(lexeme, AstNodeType.Number));
                }
                else if (lexeme.Type == LexemeType.Identifier)
                {
                    /*
                    if the token is a function then:
                    push it onto the operator stack
                    */
                    if (this.IsFunction(lexeme))
                    {
                        operatorStack.Push(new AstNode(lexeme, AstNodeType.Function));
                    }
                    else
                    {
                        astStack.Push(new AstNode(lexeme, AstNodeType.Variable));
                    }
                }
                else if (this.IsOperator(lexeme))
                {
                    /*
                    if the token is an operator, then:
                    while ((there is a function at the top of the operator stack)
                    or(there is an operator at the top of the operator stack with greater precedence)
                    or(the operator at the top of the operator stack has equal precedence and is left associative))
                    and(the operator at the top of the operator stack is not a left parenthesis):
                    pop operators from the operator stack onto the output queue.
                        push it onto the operator stack
                    */

                    AstNode node;
                    switch (lexeme.Type)
                    {
                        case LexemeType.Assign:
                            node = new AstNode(lexeme, AstNodeType.Assign);
                            break;
                        case LexemeType.Plus:
                            node = new AstNode(lexeme,
                                this.IsUnaryOperator(lexeme) ? AstNodeType.UnaryPlus : AstNodeType.BinaryPlus);
                            break;
                        case LexemeType.Minus:
                            node = new AstNode(lexeme,
                                this.IsUnaryOperator(lexeme) ? AstNodeType.UnaryMinus : AstNodeType.BinaryMinus);
                            break;
                        case LexemeType.Multiply:
                            node = new AstNode(lexeme, AstNodeType.Multiply);
                            break;
                        case LexemeType.Divide:
                            node = new AstNode(lexeme, AstNodeType.Divide);
                            break;
                        default:
                            throw new SmsCodeCompileException($"The operator {lexeme.Type} is not supported in AST.");
                    }

                    while (operatorStack.Any())
                    {
                        var top = operatorStack.Peek();
                        if (top.Type == AstNodeType.Function
                            ||
                            top.IsOperator
                            &&
                            (
                                this.GetOperatorPrecedence(top.Type) > this.GetOperatorPrecedence(node.Type)
                                ||
                                this.GetOperatorPrecedence(top.Type) == this.GetOperatorPrecedence(node.Type)
                                && this.GetOperatorAssociativity(top.Type) == OperatorAssociativity.Left
                            )
                        )
                        {
                            this.PushOperator(astStack, operatorStack.Pop());
                        }
                        else
                        {
                            break;
                        }
                    }

                    operatorStack.Push(node);
                }
                else if (lexeme.Type == LexemeType.LeftParen)
                {
                    /*
                     * if the token is a left paren (i.e. "("), then:
                        push it onto the operator stack.
                     */
                    operatorStack.Push(new AstNode(lexeme, AstNodeType.None));
                }
                else if (lexeme.Type == LexemeType.RightParen)
                {
                    /*
                     * if the token is a right paren (i.e. ")"), then:
                        while the operator at the top of the operator stack is not a left paren:
                            pop the operator from the operator stack onto the output queue.
                        if the stack runs out without finding a left paren, then there are mismatched parentheses. 
                        if there is a left paren at the top of the operator stack, then:
                            pop the operator from the operator stack and discard it
                     */
                    while (operatorStack.Any() && operatorStack.Peek().Token.Type != LexemeType.LeftParen)
                    {
                        this.PushOperator(astStack, operatorStack.Pop());
                    }

                    if (!operatorStack.Any())
                    {
                        throw new SmsCodeCompileException(
                            $"The left paren was not found for the right paren at position {lexeme.Position}");
                    }

                    operatorStack.Pop();
                }
            }

            /*
             * after while loop, if operator stack not null, pop everything to output queue
                if there are no more tokens to read then:
                while there are still operator tokens on the stack:
                if the operator token on the top of the stack is a paren, then there are mismatched parentheses.
                pop the operator from the operator stack onto the output queue
             */

            while (operatorStack.Any())
            {
                var top = operatorStack.Pop();
                if (top.Token.Type == LexemeType.LeftParen)
                {
                    throw new SmsCodeCompileException(
                        $"The right paren was not found for the left paren at position {top.Token.Position}");
                }

                this.PushOperator(astStack, top);
            }

            var prog = new AstNode(null, AstNodeType.Program);
            while (astStack.Any())
            {
                prog.Children.AddLast(astStack.Pop());
            }

            return prog;
        }

        /// <inheritdoc />
        public string Compile(string code)
        {
            var lexem = this.DoLexicalAnalysis(code);
            var ast = this.BuildAst(lexem);

            var vmCode = this.Compile(ast);
            return vmCode;
        }

        /// <inheritdoc />
        public string Compile(AstNode tree)
        {
            if (tree == null)
            {
                throw new ArgumentNullException(nameof(tree));
            }

            if (tree.Type != AstNodeType.Program)
            {
                throw new SmsCodeCompileException("Root node must be a Program node");
            }

            var sb = new StringBuilder();

            this.CompileAstRecursive(tree, sb);

            return sb.ToString();
        }

        /// <summary>
        ///     Возвращает длину целого числа в символах.
        /// </summary>
        /// <param name="code">Исходный код</param>
        /// <param name="startIndex">Индекс первой цифры числа</param>
        /// <returns>Длину целого числа в символах.</returns>
        protected int GetIntNumberLength(string code, int startIndex)
        {
            var i = startIndex;
            while (i < code.Length)
            {
                var chr = code[i];
                if (char.IsDigit(chr))
                {
                    i++;
                }
                else
                {
                    break;
                }
            }

            return i - startIndex;
        }

        /// <summary>
        ///     Возвращает длину символьного идентификатора.
        /// </summary>
        /// <param name="code">Исходный код</param>
        /// <param name="startIndex">Индекс первого символа идентификатора</param>
        /// <returns>Длина идентификатора в символах.</returns>
        protected int GetIdentifierLength(string code, int startIndex)
        {
            var i = startIndex;
            while (i < code.Length)
            {
                var chr = code[i];
                if (char.IsLetter(chr))
                {
                    i++;
                }
                else
                {
                    break;
                }
            }

            return i - startIndex;
        }

        /// <summary>
        ///     Кладет узел оператора в стек
        /// </summary>
        /// <param name="astStack">Стек Ast</param>
        /// <param name="optr">Узел оператора</param>
        private void PushOperator(Stack<AstNode> astStack, AstNode optr)
        {
            if (optr.IsUnaryOperator)
            {
                if (astStack.Count < 1)
                {
                    throw new SmsCodeCompileException(
                        $"Invalid operator '{optr.Token.Value}' at position {optr.Token.Position}");
                }

                var node1 = astStack.Pop();

                optr.Children.AddLast(node1);
            }
            else
            {
                if (astStack.Count < 2)
                {
                    throw new SmsCodeCompileException(
                        $"The operator '{optr.Token.Value}' expects 2 arguments. Position {optr.Token.Position}");
                }

                var node2 = astStack.Pop();
                var node1 = astStack.Pop();

                optr.Children.AddLast(node1);
                optr.Children.AddLast(node2);
            }

            astStack.Push(optr);
        }

        /// <summary>
        ///     Возвращает True, если лексема представляет оператор языка, иначе - False.
        /// </summary>
        /// <param name="lexeme">Лексема языка</param>
        /// <returns>True, если лексема представляет оператор языка, иначе - False.</returns>
        public bool IsOperator(Lexeme lexeme)
        {
            return lexeme.Type == LexemeType.Assign
                   || lexeme.Type == LexemeType.Minus
                   || lexeme.Type == LexemeType.Plus
                   || lexeme.Type == LexemeType.Multiply
                   || lexeme.Type == LexemeType.Divide;
        }

        /// <summary>
        ///     Проверяет, является ли лексема вызовом функции.
        /// </summary>
        /// <param name="lexeme">Лексема</param>
        /// <returns>True, если лексема является вызовом функции.</returns>
        public bool IsFunction(Lexeme lexeme)
        {
            return false;
        }

        /// <summary>
        ///     Возвращает True, если лексема представляет унарный оператор языка, иначе - False.
        /// </summary>
        /// <param name="lexeme">Лексема</param>
        /// <returns>True, если лексема представляет унарный оператор языка, иначе - False.</returns>
        public bool IsUnaryOperator(Lexeme lexeme)
        {
            if (lexeme.Type == LexemeType.Minus || lexeme.Type == LexemeType.Plus)
            {
                return
                    lexeme.Prev == null
                    || lexeme.Prev.Type == LexemeType.LeftParen
                    || this.IsOperator(lexeme.Prev);
            }

            return false;
        }

        /// <summary>
        ///     Компилирует Ast в ассемблерный код.
        /// </summary>
        /// <param name="node">Узел Ast.</param>
        /// <param name="sb"></param>
        private void CompileAstRecursive(AstNode node, StringBuilder sb)
        {
            switch (node.Type)
            {
                case AstNodeType.Variable:
                    sb.AppendLine($"push [{node.Token.Value}]");
                    break;
                case AstNodeType.Assign:
                    this.CompileAstRecursive(node.Children.Last.Value, sb);
                    sb.AppendLine($"set [{node.Children.First.Value.Token.Value}]");
                    break;
                case AstNodeType.BinaryPlus:
                    this.CompileAstRecursive(node.Children.First.Value, sb);
                    this.CompileAstRecursive(node.Children.Last.Value, sb);
                    sb.AppendLine("add");
                    break;
                case AstNodeType.UnaryPlus:
                    this.CompileAstRecursive(node.Children.Last.Value, sb);
                    break;
                case AstNodeType.BinaryMinus:
                    this.CompileAstRecursive(node.Children.First.Value, sb);
                    this.CompileAstRecursive(node.Children.Last.Value, sb);
                    sb.AppendLine("sub");
                    break;
                case AstNodeType.UnaryMinus:
                    sb.AppendLine("push 0");
                    this.CompileAstRecursive(node.Children.Last.Value, sb);
                    sb.AppendLine("sub");
                    break;
                case AstNodeType.Multiply:
                    this.CompileAstRecursive(node.Children.First.Value, sb);
                    this.CompileAstRecursive(node.Children.Last.Value, sb);
                    sb.AppendLine("mul");
                    break;
                case AstNodeType.Divide:
                    this.CompileAstRecursive(node.Children.First.Value, sb);
                    this.CompileAstRecursive(node.Children.Last.Value, sb);
                    sb.AppendLine("div");
                    break;
                case AstNodeType.Number:
                    sb.AppendLine($"push {node.Token.Value}");
                    break;
                case AstNodeType.Program:
                    foreach (var child in node.Children)
                    {
                        this.CompileAstRecursive(child, sb);
                    }

                    break;
                default:
                    throw new SmsCodeCompileException($"Invalid node {node.Type}");
            }
        }

        /// <summary>
        ///     Возвращает вес оператора.
        /// </summary>
        /// <param name="optr">Тип оператора</param>
        /// <returns>Вес оператора.</returns>
        public int GetOperatorPrecedence(AstNodeType optr)
        {
            switch (optr)
            {
                case AstNodeType.Assign:
                    return 10;
                case AstNodeType.BinaryPlus:
                case AstNodeType.BinaryMinus:
                    return 20;
                case AstNodeType.Divide:
                case AstNodeType.Multiply:
                    return 30;
                case AstNodeType.UnaryPlus:
                case AstNodeType.UnaryMinus:
                    return 40;
                default:
                    throw new SmsCodeCompileException($"The operator {optr} is not supported");
            }
        }

        /// <summary>
        ///     Возвращает ассоциативность оператора.
        /// </summary>
        /// <param name="optr">Тип оператора</param>
        /// <returns>Ассоциативность оператора.</returns>
        public OperatorAssociativity GetOperatorAssociativity(AstNodeType optr)
        {
            switch (optr)
            {
                case AstNodeType.Assign:
                case AstNodeType.UnaryPlus:
                case AstNodeType.UnaryMinus:
                    return OperatorAssociativity.Right;
                case AstNodeType.BinaryPlus:
                case AstNodeType.BinaryMinus:
                case AstNodeType.Divide:
                case AstNodeType.Multiply:
                    return OperatorAssociativity.Left;
                default:
                    throw new SmsCodeCompileException($"The operator {optr} is not supported");
            }
        }
    }
}