﻿namespace Sms.Business.Compilation
{
    /// <summary>
    ///     Тип лексемы.
    /// </summary>
    public enum LexemeType
    {
        /// <summary>
        ///     Неопределенная лексема.
        /// </summary>
        None,

        /// <summary>
        ///     Лексема "=".
        /// </summary>
        Assign,

        /// <summary>
        ///     Лексема "+".
        /// </summary>
        Plus,

        /// <summary>
        ///     Лексема "-".
        /// </summary>
        Minus,

        /// <summary>
        ///     Лексема "*".
        /// </summary>
        Multiply,

        /// <summary>
        ///     Лексема "/".
        /// </summary>
        Divide,

        /// <summary>
        ///     Лексема "(".
        /// </summary>
        LeftParen,

        /// <summary>
        ///     Лексема ")".
        /// </summary>
        RightParen,

        /// <summary>
        ///     Вещественное число.
        /// </summary>
        Number,

        /// <summary>
        ///     Идентификатор.
        /// </summary>
        Identifier
    }
}