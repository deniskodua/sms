﻿namespace Sms.Business.Compilation
{
    /// <summary>
    ///     Лексема языка.
    /// </summary>
    public class Lexeme
    {
        /// <summary>
        ///     Конструктор класса.
        /// </summary>
        /// <param name="type">Тип лексемы</param>
        /// <param name="value">Строковое значение лексемы</param>
        /// <param name="position">Позиция в строке кода</param>
        public Lexeme(LexemeType type, string value, int position)
        {
            this.Type = type;
            this.Value = value;
            this.Position = position;
        }

        /// <summary>
        ///     Строковое значение лексемы.
        /// </summary>
        public string Value { get; }

        /// <summary>
        ///     Тип лексемы.
        /// </summary>
        public LexemeType Type { get; }

        /// <summary>
        ///     Позиция в строке кода.
        /// </summary>
        public int Position { get; }

        /// <summary>
        ///     Следующая лексема.
        /// </summary>
        public Lexeme Next { get; set; }

        /// <summary>
        ///     Предыдущая лексема.
        /// </summary>
        public Lexeme Prev { get; set; }

        /// <inheritdoc />
        public override string ToString()
        {
            return
                $"{nameof(this.Position)}: {this.Position}, {nameof(this.Type)}: {this.Type}, {nameof(this.Value)}: {this.Value}";
        }
    }
}