﻿namespace Sms.Business.Compilation
{
    using Infrastructure;

    /// <summary>
    ///     Расширения для <see cref="AstNode" />
    /// </summary>
    public static class AstNodeExtensions
    {
        /// <summary>
        ///     Возвращает граф зависимостей переменных.
        /// </summary>
        /// <param name="node">Узел Ast</param>
        /// <returns>Граф зависимостей переменных</returns>
        public static Graph<string> GetVariableDependenciesGraph(this AstNode node)
        {
            var graph = new Graph<string>();

            GetVariablesGraphRecursive(graph, node, null);

            return graph;
        }

        /// <summary>
        ///     Заполняет граф зависимостей переменных.
        /// </summary>
        /// <param name="graph">Граф зависимостей переменных</param>
        /// <param name="node">Текущий ast узел</param>
        /// <param name="assignTo">Название целевой переменной</param>
        private static void GetVariablesGraphRecursive(Graph<string> graph, AstNode node, string assignTo)
        {
            if (node.Type == AstNodeType.Assign)
            {
                var variableNode = node.Children.First.Value;
                if (variableNode.Type == AstNodeType.Variable)
                {
                    assignTo = variableNode.Token.Value;
                    graph.Add(assignTo, null);

                    using (var etr = node.Children.GetEnumerator())
                    {
                        etr.MoveNext();

                        while (etr.MoveNext())
                        {
                            GetVariablesGraphRecursive(graph, etr.Current, variableNode.Token.Value);
                        }
                    }
                }
                else
                {
                    throw new SmsCodeCompileException(
                        $"At the right side of assignment must be a variable. Invalid ast node: {node}");
                }
            }
            else if (node.Type == AstNodeType.Variable)
            {
                if (!string.IsNullOrEmpty(assignTo))
                {
                    graph.Add(assignTo, node.Token.Value);
                }
            }
            else
            {
                foreach (var child in node.Children)
                {
                    GetVariablesGraphRecursive(graph, child, assignTo);
                }
            }
        }
    }
}