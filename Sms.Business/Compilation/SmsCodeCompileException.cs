﻿namespace Sms.Business.Compilation
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    ///     Ошибка компиляции кода.
    /// </summary>
    [Serializable]
    public class SmsCodeCompileException : Exception
    {
        /// <summary>
        ///     Инициализирует новый экземпляр класса <see cref="SmsCodeCompileException" />.
        /// </summary>
        public SmsCodeCompileException()
        {
        }

        /// <summary>
        ///     Инициализирует новый экземпляр класса <see cref="SmsCodeCompileException" />.
        /// </summary>
        /// <param name="message">Текст сообщения</param>
        public SmsCodeCompileException(string message)
            : base(message)
        {
        }

        /// <summary>
        ///     Инициализирует новый экземпляр класса <see cref="SmsCodeCompileException" />.
        /// </summary>
        /// <param name="message">Текст сообщения</param>
        /// <param name="innerException">Внутреннее исключение</param>
        public SmsCodeCompileException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        ///     Инициализирует новый экземпляр класса <see cref="SmsCodeCompileException" />.
        /// </summary>
        /// <param name="info">Информация о сериализации</param>
        /// <param name="context">Контекст сериализации</param>
        protected SmsCodeCompileException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}