﻿namespace Sms.Business.Compilation
{
    /// <summary>
    ///     Тип Ast узла.
    /// </summary>
    public enum AstNodeType
    {
        /// <summary>
        ///     Неопределенный тип.
        /// </summary>
        None,

        /// <summary>
        ///     Переменная.
        /// </summary>
        Variable,

        /// <summary>
        ///     Присваивание.
        /// </summary>
        Assign,

        /// <summary>
        ///     Бинарный "+"
        /// </summary>
        BinaryPlus,

        /// <summary>
        ///     Унарный "+"
        /// </summary>
        UnaryPlus,

        /// <summary>
        ///     Бинарный "-"
        /// </summary>
        BinaryMinus,

        /// <summary>
        ///     Унарный "-"
        /// </summary>
        UnaryMinus,

        /// <summary>
        ///     Умножение.
        /// </summary>
        Multiply,

        /// <summary>
        ///     Деление.
        /// </summary>
        Divide,

        /// <summary>
        ///     Число
        /// </summary>
        Number,

        /// <summary>
        ///     Функция.
        /// </summary>
        Function,

        /// <summary>
        ///     Программа.
        /// </summary>
        Program
    }
}