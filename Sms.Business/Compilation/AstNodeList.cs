﻿namespace Sms.Business.Compilation
{
    using System.Collections;
    using System.Collections.Generic;

    /// <summary>
    ///     Коллекция узлов <see cref="AstNode" />.
    /// </summary>
    public class AstNodeList : IEnumerable<AstNode>
    {
        /// <summary>
        ///     Внутреннее хранилище.
        /// </summary>
        private readonly LinkedList<AstNode> _intenalList = new LinkedList<AstNode>();

        /// <summary>
        ///     Родительский элемент.
        /// </summary>
        private readonly AstNode _parent;

        /// <summary>
        ///     Конструктор класса.
        /// </summary>
        /// <param name="parent">Родительский элемент.</param>
        public AstNodeList(AstNode parent)
        {
            this._parent = parent;
        }

        /// <summary>
        ///     Первый элемент коллекции.
        /// </summary>
        public LinkedListNode<AstNode> First => this._intenalList.First;

        /// <summary>
        ///     Последний элемент коллекции.
        /// </summary>
        public LinkedListNode<AstNode> Last => this._intenalList.Last;

        /// <summary>
        ///     Количество элементов.
        /// </summary>
        public int Count => this._intenalList.Count;

        /// <inheritdoc />
        public IEnumerator<AstNode> GetEnumerator()
        {
            return this._intenalList.GetEnumerator();
        }

        /// <inheritdoc />
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this._intenalList.GetEnumerator();
        }

        /// <summary>
        ///     Добавляет узел в конец.
        /// </summary>
        /// <param name="node">Добавляемый узел</param>
        public void AddLast(AstNode node)
        {
            node.Parent = this._parent;
            this._intenalList.AddLast(node);
        }

        /// <summary>
        ///     Удаляет последний узел из списка.
        /// </summary>
        public void RemoveLast()
        {
            if (this._intenalList.Last != null)
            {
                this._intenalList.Last.Value.Parent = null;
            }

            this._intenalList.RemoveLast();
        }

        /// <summary>
        ///     Добавляет узел в начало.
        /// </summary>
        /// <param name="node">Добавляемый узел</param>
        public void AddFirst(AstNode node)
        {
            node.Parent = this._parent;
            this._intenalList.AddFirst(node);
        }

        /// <summary>
        ///     Удаляет первый узел из списка.
        /// </summary>
        public void RemoveFirst()
        {
            if (this._intenalList.First != null)
            {
                this._intenalList.First.Value.Parent = null;
            }

            this._intenalList.RemoveFirst();
        }
    }
}