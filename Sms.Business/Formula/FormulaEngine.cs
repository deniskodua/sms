﻿namespace Sms.Business.Formula
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Reactive.Concurrency;
    using System.Reactive.Linq;
    using System.Reactive.Subjects;
    using System.Threading;
    using System.Threading.Tasks;
    using Compilation;
    using DataAccess.Models;
    using DataAccess.Repositary;
    using Infrastructure;
    using Runtime;

    /// <summary>
    ///     Компонент расчета формул.
    /// </summary>
    public class FormulaEngine : IDisposable, IFormulaEngine
    {
        /// <summary>
        ///     Компилятор кода.
        /// </summary>
        private readonly ISmsCodeCompiler _codeCmpiler;

        /// <summary>
        ///     Репозитарий формул.
        /// </summary>
        private readonly FormulaRepositary _formulaRepositary;

        /// <summary>
        ///     Очередь пересчета формул.
        /// </summary>
        private readonly Subject<FormulaCalculationJobData> _jobs;

        /// <summary>
        ///     Токен остановки цикла пересчета формул.
        /// </summary>
        private readonly CancellationTokenSource _jobsCancellationTokenSource;

        /// <summary>
        ///     Виртуальная машина для выполнения кода.
        /// </summary>
        private readonly ISmsRuntime _runtime;

        /// <summary>
        ///     Маппинг имени переменно на формулу-джобу
        /// </summary>
        private readonly ConcurrentDictionary<string, FormulaEntity> _variableName2Formula;

        /// <summary>
        ///     Объект синхронизации по имени.
        /// </summary>
        private readonly NamedReaderWriterLocker _variablesLocker;

        /// <summary>
        /// Активные джобы по расчету формул.
        /// </summary>
        private readonly ConcurrentDictionary<int, FormulaCalculationJobData> _formulaId2Job;

        /// <summary>
        /// Подписка на поток задач по расчету формул.
        /// </summary>
        private readonly IDisposable _jobSubscriptionHandle;

        /// <summary>
        ///     Конструктор класса.
        /// </summary>
        /// <param name="codeCmpiler">Компилятор кода.</param>
        /// <param name="runtime">Виртуальная машина для выполнения кода.</param>
        /// <param name="formulaRepositary">Репозитарий формул.</param>
        public FormulaEngine(ISmsCodeCompiler codeCmpiler, ISmsRuntime runtime, FormulaRepositary formulaRepositary)
        {
            this._codeCmpiler = codeCmpiler;
            this._runtime = runtime;
            this._formulaRepositary = formulaRepositary;
            this._jobs = new Subject<FormulaCalculationJobData>();

            this._jobSubscriptionHandle = this._jobs
                .ObserveOn(NewThreadScheduler.Default)
                .Buffer(TimeSpan.FromMilliseconds(100), 10)
                .Subscribe(this.ProcessFormulaQueue);

            this._jobsCancellationTokenSource = new CancellationTokenSource();
            this._variableName2Formula = new ConcurrentDictionary<string, FormulaEntity>();
            this._variablesLocker = new NamedReaderWriterLocker();
            this._formulaId2Job =
                new ConcurrentDictionary<int, FormulaCalculationJobData>();
        }

        /// <inheritdoc />
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <inheritdoc />
        public void PostJob(FormulaCalculationJobData jobData)
        {
            this._variableName2Formula.TryAdd(jobData.Formula.VariableName, jobData.Formula);

            this._jobs.OnNext(jobData);
        }

        /// <inheritdoc />
        public async Task<FormulaEntity> GetVariable(string name, int timeout = 30000, bool waitTermination = true)
        {
            if (!this._variableName2Formula.TryGetValue(name, out var formula))
            {
                return null;
            }

            Debug.WriteLine($"GetVariable.ThreadId: #{Thread.CurrentThread.ManagedThreadId}");

            var rwLock = this._variablesLocker.GetLock(name);
            var sw = new Stopwatch();
            sw.Start();
            while (sw.ElapsedMilliseconds < timeout)
            {
                rwLock.EnterReadLock();
                try
                {
                    if (!this._variableName2Formula.TryGetValue(name, out formula))
                    {
                        return null;
                    }

                    if (waitTermination && !formula.IsTerminated)
                    {
                        rwLock.ExitReadLock();
                        await Task.Delay(100);
                    }
                    else
                    {
                        return (FormulaEntity) formula.Clone();
                    }
                }
                finally
                {
                    if (rwLock.IsReadLockHeld)
                    {
                        rwLock.ExitReadLock();
                    }
                }
            }

            throw new TimeoutException($"Wait time to get variable {name} is exceeeded");
        }

        /// <summary>
        ///     Инициализация компонента.
        /// </summary>
        public virtual void Initialize()
        {
            var formulas = this._formulaRepositary.Get()
                .ToArray();

            foreach (var formula in formulas)
            {
                this._variableName2Formula.TryAdd(formula.VariableName, formula);

                if (!formula.IsTerminated)
                {
                    try
                    {
                        var lexem = this._codeCmpiler.DoLexicalAnalysis(formula.CodeText);
                        var ast = this._codeCmpiler.BuildAst(lexem);
                        var jobData = new FormulaCalculationJobData(ast, formula, ast.GetVariableDependenciesGraph());
                        this._formulaId2Job.TryAdd(formula.Id, jobData);
                    }
                    catch (Exception)
                    {
                        //TODO Logging
                    }
                }
            }

            this.ProcessFormulaQueue(null);
        }

        /// <summary>
        ///     Метод рачета формул, исполняемый фоновым потоком.
        /// </summary>
        protected virtual void ProcessFormulaQueue(IList<FormulaCalculationJobData> jobs)
        {
            Debug.WriteLine($"ProcessFormulaQueue: ThreadId: #{Thread.CurrentThread.ManagedThreadId}, JobsCount: {jobs?.Count}");

            if (jobs != null 
                && !jobs.Any())
            {
                return;
            }

            if (this._jobsCancellationTokenSource.IsCancellationRequested)
            {
                return;
            }

            if (jobs != null)
            {
                foreach (var job in jobs)
                {
                    this._formulaId2Job.TryAdd(job.Formula.Id, job);
                }
            }

            var unionGraph = new Graph<string>();
            foreach (var _ in this._formulaId2Job.Values)
            {
                unionGraph.MergeWith(_.VariableDependencies);
            }

            var cycleVertices = unionGraph.GetCycledVertices();

            foreach (var cycleVertice in cycleVertices)
            {
                if (this._variableName2Formula.TryGetValue(cycleVertice, out var formula))
                {
                    this.UpdateFormula(formula, FormulaStatus.Failed, null,
                        $"Circular reference detected for variable '{cycleVertice}'");
                }
            }

            var sort = unionGraph.TopologicalSort();

            foreach (var top in sort)
            {
                if (this._jobsCancellationTokenSource.IsCancellationRequested)
                {
                    break;
                }

                if (!this._variableName2Formula.TryGetValue(top, out var formula))
                {
                    continue;
                }

                if (formula.IsTerminated)
                {
                    continue;
                }

                double? value = null;
                var status = formula.Status;
                string errorDescription = null;

                try
                {
                    this.ProcessFormulaJob(this._formulaId2Job[formula.Id], out status, out errorDescription,
                        out value);
                }
                catch (Exception e)
                {
                    status = FormulaStatus.Failed;
                    errorDescription = e.Message;
                }
                finally
                {
                    Debug.WriteLine($"Jobs ProcessFormulaJob.Finally: #{formula}");
                    this.UpdateFormula(formula, status, value, errorDescription);
                }
            }
        }

        /// <summary>
        ///     Выполняет рачет значения формулы.
        /// </summary>
        /// <param name="formulaJob">Джоба расчеты формулы</param>
        /// <param name="status">Новый статус формулы</param>
        /// <param name="errorDescription">Описание ошибки</param>
        /// <param name="value">Значение формулы</param>
        private void ProcessFormulaJob(FormulaCalculationJobData formulaJob, out FormulaStatus status,
            out string errorDescription,
            out double? value)
        {
            errorDescription = null;
            status = formulaJob.Formula.Status;
            value = null;
            var dependentVariables = formulaJob.VariableDependencies.GetDependentVariables();
            var context = new RunCodeContext();

            foreach (var dv in dependentVariables)
            {
                if (this._variableName2Formula.TryGetValue(dv, out var depFormula))
                {
                    if (depFormula.Status == FormulaStatus.Failed)
                    {
                        status = FormulaStatus.Failed;
                        errorDescription =
                            $"Failed since dependent variable '{dv}' failed to compute";
                        break;
                    }

                    if (depFormula.Status == FormulaStatus.WaitingForDependency
                        || depFormula.Status == FormulaStatus.NotCalculated)
                    {
                        status = FormulaStatus.WaitingForDependency;
                        errorDescription =
                            $"The formula depends of variable '{dv}' which hasn't been calculated yet.";
                    }
                    else if (depFormula.Status == FormulaStatus.Calculated)
                    {
                        context.Variables.Add(dv, depFormula.VariableValue ?? 0);
                    }
                }
                else
                {
                    status = FormulaStatus.WaitingForDependency;
                    errorDescription =
                        $"The formula depends of variable '{dv}' which hasn't been calculated yet.";
                }
            }

            if (!string.IsNullOrEmpty(errorDescription))
            {
                return;
            }

            var asm = this._codeCmpiler.Compile(formulaJob.Ast);

            this._runtime.RunCode(context, asm);

            value = context.Variables[formulaJob.Formula.VariableName];

            status = FormulaStatus.Calculated;
        }

        /// <summary>
        ///     Обновляет формулу в БД.
        /// </summary>
        /// <param name="formula">Обновляемая формула</param>
        /// <param name="status">Статус формулы</param>
        /// <param name="value">Значение формулы</param>
        /// <param name="errorDescription">Описание ошибки</param>
        /// <returns>True, если успешно удалось обновить статус, иначе- False</returns>
        protected virtual void UpdateFormula(FormulaEntity formula, FormulaStatus status, double? value,
            string errorDescription)
        {
            var lck = this._variablesLocker.GetLock(formula.VariableName);
            lck.EnterWriteLock();
            try
            {
                var clone = (FormulaEntity) formula.Clone();
                clone.Status = status;
                clone.ErrorDescription = errorDescription;
                clone.VariableValue = value;
                this._formulaRepositary.Update(clone).Wait();
                formula.Status = clone.Status;
                formula.ErrorDescription = clone.ErrorDescription;
                formula.VariableValue = clone.VariableValue;

                if (formula.IsTerminated)
                {
                    this._formulaId2Job.TryRemove(formula.Id, out _);
                }
            }
            finally
            {
                lck.ExitWriteLock();
            }
        }

        /// <summary>
        ///     Очистка ресурсов.
        /// </summary>
        /// <param name="disposing">Очистка управляемых ресурсов?</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                this._jobsCancellationTokenSource.Cancel();
                this._jobSubscriptionHandle.Dispose();
                this._jobs.Dispose();
                this._jobsCancellationTokenSource.Dispose();
            }
        }
    }
}