﻿namespace Sms.Business.Formula
{
    using System.Threading.Tasks;
    using DataAccess.Models;

    /// <summary>
    ///     Интерфейс компонента поставщика формул.
    /// </summary>
    public interface IFormulaProducer
    {
        /// <summary>
        ///     Возвращает информацию о переменной.
        /// </summary>
        /// <param name="name">Название переменной.</param>
        /// <param name="timeout">Таймаут получения значения формулы</param>
        /// <param name="waitTermination">True, если необходимо ждать расчета переменной.</param>
        /// <returns>Информация о переменной</returns>
        Task<FormulaEntity> GetVariable(string name, int timeout = 30000, bool waitTermination = true);

        /// <summary>
        ///     Добавляет новую формулу для расчета.
        /// </summary>
        /// <param name="formulaText">Код формулы</param>
        /// <returns></returns>
        Task AddFormula(string formulaText);
    }
}