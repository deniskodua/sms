﻿namespace Sms.Business.Formula
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Compilation;
    using DataAccess.Models;
    using DataAccess.Repositary;

    /// <summary>
    ///     Поставщик формул.
    /// </summary>
    public class FormulaProducer : IFormulaProducer
    {
        /// <summary>
        ///     Компилятор кода.
        /// </summary>
        private readonly ISmsCodeCompiler _codeCmpiler;

        /// <summary>
        ///     Механизм расчета формул.
        /// </summary>
        private readonly IFormulaEngine _formulaEngine;

        /// <summary>
        ///     Репозитарий формул.
        /// </summary>
        private readonly FormulaRepositary _formulaRepositary;

        /// <summary>
        ///     Конструктор класса.
        /// </summary>
        /// <param name="codeCmpiler">Компилятор кода.</param>
        /// <param name="formulaEngine">Механизм расчета формул.</param>
        /// <param name="formulaRepositary">Репозитарий формул</param>
        public FormulaProducer(ISmsCodeCompiler codeCmpiler, IFormulaEngine formulaEngine,
            FormulaRepositary formulaRepositary)
        {
            this._codeCmpiler = codeCmpiler;
            this._formulaEngine = formulaEngine;
            this._formulaRepositary = formulaRepositary;
        }

        /// <inheritdoc />
        public async Task<FormulaEntity> GetVariable(string name, int timeout = 30000, bool waitTermination = true)
        {
            return await this._formulaEngine.GetVariable(name, timeout, waitTermination);
        }

        /// <inheritdoc />
        public virtual async Task AddFormula(string formulaText)
        {
            var lexem = this._codeCmpiler.DoLexicalAnalysis(formulaText);
            var ast = this._codeCmpiler.BuildAst(lexem);
            var depGraph = ast.GetVariableDependenciesGraph();

            if (!depGraph.SourceVertices.Any())
            {
                throw new NotSupportedException("The formula you posted does not contain variable assignment");
            }

            var variableName = depGraph.SourceVertices.First();

            var formula = new FormulaEntity
            {
                CodeText = formulaText,
                Status = FormulaStatus.NotCalculated,
                VariableName = variableName
            };

            if (await this._formulaEngine.GetVariable(variableName, waitTermination: false) != null)
            {
                throw new InvalidOperationException($"The variable '{variableName}' is already declared in other formula. Please, specify another name.");
            }

            await this._formulaRepositary.Create(formula);

            this._formulaEngine.PostJob(new FormulaCalculationJobData(ast, formula, depGraph));
        }
    }
}