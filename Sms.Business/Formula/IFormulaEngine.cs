﻿namespace Sms.Business.Formula
{
    using System.Threading.Tasks;
    using DataAccess.Models;

    /// <summary>
    ///     Компонент для расчета формул.
    /// </summary>
    public interface IFormulaEngine
    {
        /// <summary>
        ///     Отправляет задания пересчета формулы.
        /// </summary>
        /// <param name="jobData">Данные джобы</param>
        void PostJob(FormulaCalculationJobData jobData);

        /// <summary>
        ///     Возвращает информацию о переменной.
        /// </summary>
        /// <param name="name">Название переменной.</param>
        /// <param name="timeout">Таймаут получения значения формулы</param>
        /// <param name="waitTermination">True, если необходимо ждать расчета переменной.</param>
        /// <returns>Информация о переменной</returns>
        Task<FormulaEntity> GetVariable(string name, int timeout = 30000, bool waitTermination = true);
    }
}