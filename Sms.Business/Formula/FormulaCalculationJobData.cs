﻿namespace Sms.Business.Formula
{
    using Compilation;
    using DataAccess.Models;
    using Infrastructure;

    /// <summary>
    ///     Данные задания расчета формулы.
    /// </summary>
    public class FormulaCalculationJobData
    {
        /// <summary>
        ///     Конструктор класса.
        /// </summary>
        /// <param name="ast">Ast корень формулы</param>
        /// <param name="formula">Формула</param>
        /// <param name="variableDependencies">Граф зависимостей переменных</param>
        public FormulaCalculationJobData(AstNode ast, FormulaEntity formula, Graph<string> variableDependencies)
        {
            this.Ast = ast;
            this.Formula = formula;
            this.VariableDependencies = variableDependencies;
        }

        /// <summary>
        ///     Ast корень формулы.
        /// </summary>
        public AstNode Ast { get; }

        /// <summary>
        ///     Формула.
        /// </summary>
        public FormulaEntity Formula { get; }

        /// <summary>
        ///     Граф зависимостей переменных.
        /// </summary>
        public Graph<string> VariableDependencies { get; }
    }
}