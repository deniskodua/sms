﻿namespace Sms.Business.Infrastructure
{
    using System.Collections.Concurrent;
    using System.Threading;

    /// <summary>
    ///     Реализует возможность блокировки ресурса по имени (multiple-read-single-write)
    /// </summary>
    public class NamedReaderWriterLocker
    {
        /// <summary>
        ///     Словарь созданных объектов синхронизации.
        /// </summary>
        private readonly ConcurrentDictionary<string, ReaderWriterLockSlim> _lockDict =
            new ConcurrentDictionary<string, ReaderWriterLockSlim>();

        /// <summary>
        ///     Возвращает объект синхронизации по имени.
        /// </summary>
        /// <param name="name">Имя объекта синхронизации</param>
        /// <returns>Объект синхронизации</returns>
        public ReaderWriterLockSlim GetLock(string name)
        {
            return this._lockDict.GetOrAdd(name, s => new ReaderWriterLockSlim());
        }
    }
}