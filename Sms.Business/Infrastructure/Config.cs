﻿namespace Sms.Business.Infrastructure
{
    /// <summary>
    ///     Класс настроек проекта.
    /// </summary>
    public class Config : IConfig
    {
        /// <summary>
        ///     Строка подключения к БД.
        /// </summary>
        public string SmsDbConnectionString { get; set; }
    }
}