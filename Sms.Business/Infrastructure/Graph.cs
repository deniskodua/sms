﻿namespace Sms.Business.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    ///     Граф зависимостей переменных формулы.
    /// </summary>
    public class Graph<TVertice> : ICloneable
        where TVertice : IEquatable<TVertice>
    {
        /// <summary>
        ///     Матрица смежности.
        /// </summary>
        private Dictionary<TVertice, HashSet<TVertice>>
            _adjacencyMatrix = new Dictionary<TVertice, HashSet<TVertice>>();

        /// <summary>
        ///     Все вершины графа.
        /// </summary>
        private HashSet<TVertice> _allVertices = new HashSet<TVertice>();

        /// <summary>
        ///     Все source вершины.
        /// </summary>
        private HashSet<TVertice> _sourceVertices = new HashSet<TVertice>();

        /// <summary>
        ///     Все присваиваемые переменные.
        /// </summary>
        public IEnumerable<TVertice> SourceVertices => this._sourceVertices;

        /// <summary>
        ///     Все вершины графа.
        /// </summary>
        public IEnumerable<TVertice> AllVertices => this._allVertices;

        /// <inheritdoc />
        public object Clone()
        {
            return new Graph<TVertice>
            {
                _adjacencyMatrix =
                    this._adjacencyMatrix.ToDictionary(_ => _.Key, _ => new HashSet<TVertice>(_.Value)),
                _allVertices = new HashSet<TVertice>(this._allVertices),
                _sourceVertices = new HashSet<TVertice>(this._sourceVertices)
            };
        }

        /// <summary>
        ///     Добавление связи от вершины <paramref name="source" /> к <paramref name="target" />
        /// </summary>
        /// <param name="source">Исходная вершина</param>
        /// <param name="target">Целевая вершина</param>
        public void Add(TVertice source, TVertice target)
        {
            if (!this._adjacencyMatrix.TryGetValue(source, out var hs))
            {
                hs = new HashSet<TVertice>();
                this._adjacencyMatrix.Add(source, hs);
            }

            this._sourceVertices.Add(source);

            this._allVertices.Add(source);
            if (target != null)
            {
                hs.Add(target);
                if (!this._adjacencyMatrix.TryGetValue(target, out _))
                {
                    this._adjacencyMatrix.Add(target, new HashSet<TVertice>());
                }
            }
        }

        /// <summary>
        ///     Возвращает коллекцию зависимых переменных.
        /// </summary>
        /// <returns>Коллекция зависимых переменных.</returns>
        public IEnumerable<TVertice> GetDependentVariables()
        {
            var result = new HashSet<TVertice>();

            foreach (var kv in this._adjacencyMatrix)
            {
                result.UnionWith(kv.Value);
            }

            return result;
        }

        public HashSet<TVertice> GetCycledVertices()
        {
            var result = new HashSet<TVertice>();
            var recursionSet = new HashSet<TVertice>();
            var visited = new HashSet<TVertice>();

            foreach (var vertice in this._allVertices)
            {
                visited.Clear();
                if (!result.Contains(vertice))
                {
                    if (this.IsCyclicRecursive(visited, recursionSet, vertice))
                    {
                        result.Add(vertice);
                    }
                }
            }

            return result;
        }

        /// <summary>
        ///     Возвращает True, если в графе есть цикл, иначе - False.
        /// </summary>
        /// <returns>True, если в графе есть цикл, иначе - False.</returns>
        public bool IsCyclic()
        {
            var visited = new HashSet<TVertice>();
            var recursionSet = new HashSet<TVertice>();

            foreach (var vertice in this._allVertices)
            {
                if (!visited.Contains(vertice))
                {
                    if (this.IsCyclicRecursive(visited, recursionSet, vertice))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        ///     Возвращает True, если в графе есть цикл, иначе - False.
        /// </summary>
        /// <param name="visited">Множество посещенных вершин</param>
        /// <param name="recursionSet">Стек рекурсии</param>
        /// <param name="vertice">Текущая вершина</param>
        /// <returns>True, если в графе есть цикл, иначе - False.</returns>
        public bool IsCyclicRecursive(HashSet<TVertice> visited, HashSet<TVertice> recursionSet, TVertice vertice)
        {
            visited.Add(vertice);
            recursionSet.Add(vertice);

            foreach (var adjVertice in this._adjacencyMatrix[vertice])
            {
                if (!visited.Contains(adjVertice))
                {
                    if (this.IsCyclicRecursive(visited, recursionSet, adjVertice))
                    {
                        return true;
                    }
                }
                else if (recursionSet.Contains(adjVertice))
                {
                    return true;
                }
            }

            recursionSet.Remove(vertice);
            return false;
        }

        /// <summary>
        ///     Производит топологическую сортировку графа.
        /// </summary>
        /// <returns>Последовательность отсортированных вершин графа</returns>
        public IEnumerable<TVertice> TopologicalSort()
        {
            var visited = new HashSet<TVertice>();
            var list = new List<TVertice>();

            foreach (var vertice in this._allVertices)
            {
                if (!visited.Contains(vertice))
                {
                    this.TopologicalSortRecursive(visited, list, vertice);
                }
            }

            return list;
        }

        /// <summary>
        ///     Производит топологическую сортировку графа.
        /// </summary>
        /// <param name="visited">Список посещенных вершин</param>
        /// <param name="list">Результат сортировки</param>
        /// <param name="vertice">Текущая вершина</param>
        private void TopologicalSortRecursive(HashSet<TVertice> visited, List<TVertice> list, TVertice vertice)
        {
            visited.Add(vertice);
            foreach (var adjVertice in this._adjacencyMatrix[vertice])
            {
                if (!visited.Contains(adjVertice))
                {
                    this.TopologicalSortRecursive(visited, list, adjVertice);
                }
            }

            list.Add(vertice);
        }

        /// <summary>
        ///     Выполняет слияние текущего графа с <paramref name="graph" />
        /// </summary>
        /// <param name="graph"></param>
        public void MergeWith(Graph<TVertice> graph)
        {
            foreach (var m in graph._adjacencyMatrix)
            {
                if (this._adjacencyMatrix.ContainsKey(m.Key))
                {
                    this._adjacencyMatrix[m.Key].UnionWith(m.Value);
                }
                else
                {
                    this._adjacencyMatrix.Add(m.Key, new HashSet<TVertice>(m.Value));
                }
            }

            this._allVertices.UnionWith(graph._allVertices);

            this._sourceVertices.UnionWith(graph._sourceVertices);
        }
    }
}