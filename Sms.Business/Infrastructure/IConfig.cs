﻿namespace Sms.Business.Infrastructure
{
    /// <summary>
    ///     Интерфейс класса настроек проекта
    /// </summary>
    public interface IConfig
    {
        /// <summary>
        ///     Строка подключения к БД.
        /// </summary>
        string SmsDbConnectionString { get; }
    }
}