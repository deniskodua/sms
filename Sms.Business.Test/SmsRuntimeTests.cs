﻿namespace Sms.Business.Test
{
    using Compilation;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Runtime;

    /// <summary>
    ///     Тесты для <see cref="SmsRuntime" />
    /// </summary>
    [TestClass]
    public class SmsRuntimeTests
    {
        /// <summary>
        ///     Тест метода <see cref="SmsRuntime.RunCode" />
        /// </summary>
        [TestMethod]
        public void RunCode()
        {
            var rt = new SmsRuntime();
            var compiler = new SmsCodeCompiler();

            var context = new RunCodeContext();
            var asm = compiler.Compile("a=1");
            Assert.AreEqual(
                @"push 1
set [a]
"
                , asm);
            rt.RunCode(context, asm);
            Assert.AreEqual(1.0, context.Variables["a"]);
/******************************************************************************/
            context = new RunCodeContext();
            asm = compiler.Compile("a = -2 * 3");
            Assert.AreEqual(
                @"push 0
push 2
sub
push 3
mul
set [a]
"
                , asm);
            rt.RunCode(context, asm);
            Assert.AreEqual(-6.0, context.Variables["a"]);
/******************************************************************************/
            context = new RunCodeContext();
            context.Variables.Add("b", 5.5);
            context.Variables.Add("c", -10);

            asm = compiler.Compile("a = -(4.5 + b) * c");
            Assert.AreEqual(
                @"push 0
push 4.5
push [b]
add
sub
push [c]
mul
set [a]
"
                , asm);

            rt.RunCode(context, asm);
            Assert.AreEqual(100, context.Variables["a"]);
            /******************************************************************************/
            context = new RunCodeContext();
            context.Variables.Add("b", 5.5);
            context.Variables.Add("c", -10);

            asm = compiler.Compile("a = (4.5 + b) / -c");
            Assert.AreEqual(
                @"push 4.5
push [b]
add
push 0
push [c]
sub
div
set [a]
"
                , asm);

            rt.RunCode(context, asm);
            Assert.AreEqual(1, context.Variables["a"]);
        }
    }
}