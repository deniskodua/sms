﻿namespace Sms.Business.Test
{
    using System.Linq;
    using Compilation;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    ///     Тесты для <see cref="SmsCodeCompiler" />
    /// </summary>
    [TestClass]
    public class SmsCodeCompilerTests
    {
        /// <summary>
        ///     Тест метода <see cref="SmsCodeCompiler.DoLexicalAnalysis" />
        /// </summary>
        [TestMethod]
        public void DoLexicalAnalysis()
        {
            var cc = new SmsCodeCompiler();
            this.CheckLexemeChain(
                cc.DoLexicalAnalysis(" "));

            this.CheckLexemeChain(cc.DoLexicalAnalysis("a=1"),
                new Lexeme(LexemeType.Identifier, "a", 0),
                new Lexeme(LexemeType.Assign, "=", 1),
                new Lexeme(LexemeType.Number, "1", 2)
            );

            this.CheckLexemeChain(cc.DoLexicalAnalysis("a=-1"),
                new Lexeme(LexemeType.Identifier, "a", 0),
                new Lexeme(LexemeType.Assign, "=", 1),
                new Lexeme(LexemeType.Minus, "-", 2),
                new Lexeme(LexemeType.Number, "1", 3));

            this.CheckLexemeChain(cc.DoLexicalAnalysis(" a = 1 "),
                new Lexeme(LexemeType.Identifier, "a", 1),
                new Lexeme(LexemeType.Assign, "=", 3),
                new Lexeme(LexemeType.Number, "1", 5)
            );

            this.CheckLexemeChain(cc.DoLexicalAnalysis("a=1.569"),
                new Lexeme(LexemeType.Identifier, "a", 0),
                new Lexeme(LexemeType.Assign, "=", 1),
                new Lexeme(LexemeType.Number, "1.569", 2)
            );

            this.CheckLexemeChain(cc.DoLexicalAnalysis("a= c * (0.0156 + 3.14) / b"),
                new Lexeme(LexemeType.Identifier, "a", 0),
                new Lexeme(LexemeType.Assign, "=", 1),
                new Lexeme(LexemeType.Identifier, "c", 3),
                new Lexeme(LexemeType.Multiply, "*", 5),
                new Lexeme(LexemeType.LeftParen, "(", 7),
                new Lexeme(LexemeType.Number, "0.0156", 8),
                new Lexeme(LexemeType.Plus, "+", 15),
                new Lexeme(LexemeType.Number, "3.14", 17),
                new Lexeme(LexemeType.RightParen, ")", 21),
                new Lexeme(LexemeType.Divide, "/", 23),
                new Lexeme(LexemeType.Identifier, "b", 25)
            );

            var ex = Assert.ThrowsException<SmsCodeCompileException>(() =>
                this.CheckLexemeChain(cc.DoLexicalAnalysis("a=f % 2")));
        }

        /// <summary>
        ///     Тест метода <see cref="SmsCodeCompiler.BuildAst" />
        /// </summary>
        [TestMethod]
        public void BuildAst()
        {
            var cc = new SmsCodeCompiler();

            this.CheckAst(
                cc.BuildAst(cc.DoLexicalAnalysis("a = 1")),
                new AstNode(null, AstNodeType.Program).AddChild(
                    new AstNode(new Lexeme(LexemeType.Assign, "=", 1), AstNodeType.Assign).AddChild(
                        new AstNode(new Lexeme(LexemeType.Identifier, "a", 0), AstNodeType.Variable),
                        new AstNode(new Lexeme(LexemeType.Number, "1", 2), AstNodeType.Number))
                )
            );

            this.CheckAst(
                cc.BuildAst(cc.DoLexicalAnalysis("a = -1")),
                new AstNode(null, AstNodeType.Program).AddChild(
                    new AstNode(new Lexeme(LexemeType.Assign, "=", 1), AstNodeType.Assign).AddChild(
                        new AstNode(new Lexeme(LexemeType.Identifier, "a", 0), AstNodeType.Variable),
                        new AstNode(new Lexeme(LexemeType.Minus, "-", 0), AstNodeType.UnaryMinus).AddChild(
                            new AstNode(new Lexeme(LexemeType.Number, "1", 2), AstNodeType.Number))
                    )
                )
            );

            this.CheckAst(
                cc.BuildAst(cc.DoLexicalAnalysis("a = +1")),
                new AstNode(null, AstNodeType.Program).AddChild(
                    new AstNode(new Lexeme(LexemeType.Assign, "=", 1), AstNodeType.Assign).AddChild(
                        new AstNode(new Lexeme(LexemeType.Identifier, "a", 0), AstNodeType.Variable),
                        new AstNode(new Lexeme(LexemeType.Plus, "+", 0), AstNodeType.UnaryPlus).AddChild(
                            new AstNode(new Lexeme(LexemeType.Number, "1", 2), AstNodeType.Number))
                    )
                )
            );

            this.CheckAst(
                cc.BuildAst(cc.DoLexicalAnalysis("a = -(c - d)")),
                new AstNode(null, AstNodeType.Program).AddChild(
                    new AstNode(new Lexeme(LexemeType.Assign, "=", 1), AstNodeType.Assign).AddChild(
                        new AstNode(new Lexeme(LexemeType.Identifier, "a", 0), AstNodeType.Variable),
                        new AstNode(new Lexeme(LexemeType.Minus, "-", 0), AstNodeType.UnaryMinus).AddChild(
                            new AstNode(new Lexeme(LexemeType.Minus, "-", 0), AstNodeType.BinaryMinus).AddChild(
                                new AstNode(new Lexeme(LexemeType.Identifier, "c", 2), AstNodeType.Variable),
                                new AstNode(new Lexeme(LexemeType.Identifier, "d", 2), AstNodeType.Variable))
                        )
                    )
                )
            );

            this.CheckAst(
                cc.BuildAst(cc.DoLexicalAnalysis("a = 3.1 + 4 * 2 / ( 1 - 5 ) * 2")),
                new AstNode(null, AstNodeType.Program).AddChild
                (
                    new AstNode(new Lexeme(LexemeType.Assign, "=", 1), AstNodeType.Assign).AddChild
                    (
                        new AstNode(new Lexeme(LexemeType.Identifier, "a", 0), AstNodeType.Variable),
                        new AstNode(new Lexeme(LexemeType.Plus, "+", 0), AstNodeType.BinaryPlus).AddChild
                        (
                            new AstNode(new Lexeme(LexemeType.Number, "3.1", 0), AstNodeType.Number),
                            new AstNode(new Lexeme(LexemeType.Multiply, "*", 0), AstNodeType.Multiply)
                                .AddChild
                                (
                                    new AstNode(new Lexeme(LexemeType.Divide, "/", 0), AstNodeType.Divide).AddChild
                                    (
                                        new AstNode(new Lexeme(LexemeType.Multiply, "*", 0), AstNodeType.Multiply)
                                            .AddChild
                                            (
                                                new AstNode(new Lexeme(LexemeType.Number, "4", 0), AstNodeType.Number),
                                                new AstNode(new Lexeme(LexemeType.Number, "2", 0), AstNodeType.Number)
                                            ),
                                        new AstNode(new Lexeme(LexemeType.Minus, "-", 0), AstNodeType.BinaryMinus)
                                            .AddChild
                                            (
                                                new AstNode(new Lexeme(LexemeType.Number, "1", 0), AstNodeType.Number),
                                                new AstNode(new Lexeme(LexemeType.Number, "5", 0), AstNodeType.Number)
                                            )
                                    ),
                                    new AstNode(new Lexeme(LexemeType.Number, "2", 0), AstNodeType.Number)
                                )
                        )
                    )
                )
            );

            var ex = Assert.ThrowsException<SmsCodeCompileException>(() =>
                cc.BuildAst(cc.DoLexicalAnalysis("a = (b - c)) * 2")));

            Assert.AreEqual("The left paren was not found for the right paren at position 11", ex.Message);

            ex = Assert.ThrowsException<SmsCodeCompileException>(() =>
                cc.BuildAst(cc.DoLexicalAnalysis("a = ((b - c) * 2")));

            Assert.AreEqual("The right paren was not found for the left paren at position 4", ex.Message);

            ex = Assert.ThrowsException<SmsCodeCompileException>(() =>
                cc.BuildAst(cc.DoLexicalAnalysis("a = *3")));

            Assert.AreEqual("The operator '=' expects 2 arguments. Position 2", ex.Message);
        }

        /// <summary>
        ///     Проверяет соответствие лексем.
        /// </summary>
        /// <param name="expected">Ожидаемый результат</param>
        /// <param name="actual">Реальный результат</param>
        private void CheckLexeme(Lexeme expected, Lexeme actual)
        {
            Assert.AreEqual(expected.Type, actual.Type);
            Assert.AreEqual(expected.Position, actual.Position);
            Assert.AreEqual(expected.Value, actual.Value);
        }

        /// <summary>
        ///     Проверяет соответствие лексем.
        /// </summary>
        /// <param name="actual">Ожидаемый результат</param>
        /// <param name="expected">Реальный результат</param>
        private void CheckLexemeChain(Lexeme actual, params Lexeme[] expected)
        {
            for (var i = 0; i < expected.Length - 1; i++)
            {
                expected[i].Next = expected[i + 1];
                expected[i + 1].Prev = expected[i];
            }

            this.CheckLexemeChain(actual, expected.Length > 0 ? expected[0] : null);
        }

        /// <summary>
        ///     Проверяет соответствие лексем.
        /// </summary>
        /// <param name="actual">Ожидаемый результат</param>
        /// <param name="expected">Реальный результат</param>
        private void CheckLexemeChain(Lexeme actual, Lexeme expected)
        {
            if (actual == null)
            {
                if (expected == null)
                {
                    return;
                }

                throw new AssertFailedException($"Actual: {actual}, Expected: {expected}");
            }

            if (expected == null)
            {
                throw new AssertFailedException($"Actual: {actual}, Expected: {expected}");
            }

            for (; expected != null && actual != null; expected = expected.Next, actual = actual.Next)
            {
                this.CheckLexeme(expected, actual);
            }

            if (expected == null && actual != null)
            {
                throw new AssertFailedException($"There is one more actual Lexeme {actual}");
            }

            if (expected != null && actual == null)
            {
                throw new AssertFailedException($"There is one more expected Lexeme {expected}");
            }
        }

        /// <summary>
        ///     Проверяет соответствие AST.
        /// </summary>
        /// <param name="actual">Ожидаемый результат</param>
        /// <param name="expected">Реальный результат</param>
        private void CheckAst(AstNode actual, AstNode expected)
        {
            Assert.AreEqual(expected.Token?.Value, actual.Token?.Value);
            Assert.AreEqual(expected.Token?.Type, actual.Token?.Type);
            Assert.AreEqual(expected.Type, actual.Type);
            Assert.AreEqual(expected.Children.Count, actual.Children.Count);

            var expectedArr = expected.Children.ToArray();
            var actualArr = actual.Children.ToArray();
            for (var i = 0; i < expected.Children.Count; i++)
            {
                this.CheckAst(actualArr[i], expectedArr[i]);
            }
        }
    }
}