﻿namespace Sms.Business.Test
{
    using System;
    using System.Linq;
    using System.Reflection;
    using System.Threading;
    using System.Threading.Tasks;
    using Autofac;
    using DataAccess.Models;
    using DataAccess.Repositary;
    using FormulaServer;
    using Moq;

    /// <summary>
    ///     Базовый тестовый класс.
    /// </summary>
    public class TestBase
    {
        /// <summary>
        ///     Настройки подключения к сайт-коллекции.
        /// </summary>
        protected static readonly Setting SettingInfo = SettingContext.ResolveSetting("Sms");

        /// <summary>
        ///     Счетчик ИД.
        /// </summary>
        private static int _counter;

        /// <summary>
        ///     Создает тестовый контейнер.
        /// </summary>
        /// <returns>Ioc контейнер.</returns>
        protected ILifetimeScope CreateContainer(Action<ContainerBuilder> registrationOverrides = null)
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule(new BusinessAutofacModule(SettingInfo.SmsCodeDbConnectionString));

            registrationOverrides?.Invoke(builder);

            var container = builder.Build();
            return container;
        }

        /// <summary>
        ///     Создает Mock для указанного типа
        /// </summary>
        /// <typeparam name="TType">Тип класса</typeparam>
        /// <param name="container">Контейнер зависимостей</param>
        /// <returns>Mock типа</returns>
        protected Mock<TType> NewMock<TType>(IComponentContext container)
            where TType : class
        {
            return (Mock<TType>) this.NewMock(typeof(TType), container);
        }

        /// <summary>
        ///     Создает Mock для указанного типа
        /// </summary>
        /// <param name="type">Тип или интерфейс</param>
        /// <param name="container">Контейнер зависимостей</param>
        /// <returns>Mock типа</returns>
        protected Mock NewMock(Type type, IComponentContext container)
        {
            var constructor = type.GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public)
                .First();
            var parameterTypes = constructor.GetParameters()
                .Select(_ => container.Resolve(_.ParameterType))
                .ToArray();

            var mock = (Mock) typeof(Mock<>).MakeGenericType(type).GetConstructor(
                    new[]
                    {
                        typeof(MockBehavior), typeof(object[])
                    })
                ?.Invoke(
                    new object[]
                    {
                        MockBehavior.Loose, parameterTypes
                    });
            mock.CallBase = true;
            return mock;
        }

        /// <summary>
        ///     Создает фейковый репозитарий для <typeparamref name="TRepositary" />
        /// </summary>
        /// <typeparam name="TRepositary">Тип репозитария</typeparam>
        /// <typeparam name="TEntity">Тип сущности</typeparam>
        /// <param name="builder"></param>
        protected void InjectMockRepositary<TRepositary, TEntity>(ContainerBuilder builder)
            where TRepositary : BaseRepositary<TEntity>
            where TEntity : class, IEntity
        {
            builder.Register(c =>
                {
                    var mock = this.NewMock<TRepositary>(c);
                    mock.Setup(r => r.Update(It.IsAny<TEntity>()))
                        .Returns(Task.CompletedTask);
                    mock.Setup(r => r.Create(It.IsAny<TEntity>()))
                        .Callback<TEntity>(_ => _.Id = Interlocked.Increment(ref _counter))
                        .Returns(Task.CompletedTask);
                    mock.Setup(r => r.Delete(It.IsAny<TEntity>()))
                        .Returns(Task.CompletedTask);
                    mock.Setup(r => r.Get())
                        .Returns(Enumerable.Empty<TEntity>().AsQueryable());
                    return mock.Object;
                })
                .AsSelf()
                .InstancePerLifetimeScope();
        }
    }
}