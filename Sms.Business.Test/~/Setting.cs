﻿// Copyright © 2018. i-Sys. All rights reserved.

namespace Sms.Business.Test
{
    /// <summary>
    ///     Описывает девелоперские настройки для решения.
    /// </summary>
    public class Setting
    {
        /// <summary>
        ///     Строка подключения к БД
        /// </summary>
        public string SmsCodeDbConnectionString { get; set; }
    }
}