﻿// Copyright © 2018. i-Sys. All rights reserved.

namespace Sms.Business.Test
{
    using System;
    using System.IO;
    using System.Reflection;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    /// <summary>
    ///     Контекст получения девелоперских настроек.
    /// </summary>
    public static class SettingContext
    {
        /// <summary>
        ///     Возвращает настройки по указанному типу источника.
        /// </summary>
        /// <param name="settingType">
        ///     Тип источника.
        /// </param>
        /// <returns>
        ///     Настройки.
        /// </returns>
        public static Setting ResolveSetting(string settingType)
        {
            var directoryInfo = new DirectoryInfo(Assembly.GetExecutingAssembly().Location);
            while (directoryInfo != null && !Directory.Exists(Path.Combine(directoryInfo.FullName, "SolutionItems")))
            {
                directoryInfo = directoryInfo.Parent;
            }

            if (directoryInfo == null)
            {
                throw new NullReferenceException("Directory \"SolutionItems\" not found.");
            }

            var settingPath = Path.Combine(directoryInfo.FullName, "SolutionItems\\SlnSettings.user.json");

            if (!File.Exists(settingPath))
            {
                throw new NullReferenceException("Solution Items\\SlnSettings.user.json not found.");
            }

            var jObj = JObject.Parse(File.ReadAllText(settingPath));

            if (jObj[settingType] == null)
            {
                throw new NullReferenceException($"The SettingType={settingType} was not found.");
            }

            return JsonConvert.DeserializeObject<Setting>(jObj[settingType].ToString());
        }
    }
}