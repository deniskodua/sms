﻿namespace Sms.Business.Test
{
    using System.Collections.Generic;
    using System.Linq;
    using Infrastructure;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    ///     Тесты для <see cref="Graph{TVertice}" />
    /// </summary>
    [TestClass]
    public class VariablesGraphTests : TestBase
    {
        /// <summary>
        ///     Тест метода <see cref="Graph{TVertice}.IsCyclic" />
        /// </summary>
        [TestMethod]
        public void DetectCycle()
        {
            var g = new Graph<string>();
            g.Add("a", "a");
            Assert.IsTrue(g.IsCyclic());
            this.AssertTheSame(g.GetCycledVertices(), "a");

            g = new Graph<string>();
            g.Add("a", "b");
            g.Add("b", "a");
            Assert.IsTrue(g.IsCyclic());
            this.AssertTheSame(g.GetCycledVertices(), "a", "b");

            g = new Graph<string>();
            g.Add("a", "b");
            g.Add("b", "c");
            g.Add("c", "a");
            Assert.IsTrue(g.IsCyclic());
            this.AssertTheSame(g.GetCycledVertices(), "a", "b", "c");

            g = new Graph<string>();
            g.Add("a", "b");
            Assert.IsFalse(g.IsCyclic());
            this.AssertTheSame(g.GetCycledVertices());

            g = new Graph<string>();
            g.Add("a", "");
            Assert.IsFalse(g.IsCyclic());
            this.AssertTheSame(g.GetCycledVertices());

            g = new Graph<string>();
            g.Add("a", "b");
            g.Add("b", "c");
            g.Add("d", "c");
            Assert.IsFalse(g.IsCyclic());
            this.AssertTheSame(g.GetCycledVertices());
        }

        /// <summary>
        ///     Тест метода <see cref="Graph{TVertice}.TopologicalSort" />
        /// </summary>
        [TestMethod]
        public void TopologicalSort()
        {
            var g = new Graph<string>();
            g.Add("a", null);
            this.CheckOrder(g.TopologicalSort(), "a");

            g = new Graph<string>();
            g.Add("a", "b");
            this.CheckOrder(g.TopologicalSort(), "b", "a");

            g = new Graph<string>();
            g.Add("a", "b");
            g.Add("a", "c");
            g.Add("b", "d");
            g.Add("c", "d");
            g.Add("e", "c");
            g.Add("e", "d");
            this.CheckOrder(g.TopologicalSort(), "d", "b", "c", "a", "e");
        }

        /// <summary>
        ///     Тест метода <see cref="Graph{TVertice}.MergeWith" />
        /// </summary>
        [TestMethod]
        public void MergeWith()
        {
            var g = new Graph<string>();
            g.Add("a", null);
            var g2 = new Graph<string>();
            g2.MergeWith(g);

            Assert.AreEqual(1, g2.AllVertices.Count());
            Assert.AreEqual("a", g2.SourceVertices.Single());
        }

        /// <summary>
        ///     Проверяет соттветствие вершин
        /// </summary>
        /// <param name="items">Реальный результат</param>
        /// <param name="vertexes">Ожидаемый</param>
        private void CheckOrder(IEnumerable<string> items, params string[] vertexes)
        {
            Assert.AreEqual(vertexes.Length, items.Count());

            var i = -1;
            foreach (var item in items)
            {
                i++;
                Assert.AreEqual(vertexes[i], item);
            }
        }

        /// <summary>
        /// Сравнивает множество со значениями
        /// </summary>
        /// <param name="set">Множество</param>
        /// <param name="values">Список значений</param>
        private void AssertTheSame(HashSet<string> set, params string[] values)
        {
            Assert.AreEqual(values.Length, set.Count);
            foreach (var val in values)
            {
                Assert.IsTrue(set.Contains(val));
            }
        }
    }
}