﻿namespace Sms.Business.Test
{
    using System;
    using System.Diagnostics;
    using System.Threading;
    using Autofac;
    using Compilation;
    using DataAccess.Models;
    using DataAccess.Repositary;
    using Formula;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    ///     Тесты для <see cref="FormulaEngine" />, <see cref="FormulaProducer" />
    /// </summary>
    [TestClass]
    public class FormulaEngineTests : TestBase
    {
        /// <summary>
        ///     Проверяет корректность обработки формулы "a=1"
        /// </summary>
        [TestMethod]
        public void Send_A_EqualTo_Constant()
        {
            using (var container = this.CreateContainer(this.InjectMockRepositary<FormulaRepositary, FormulaEntity>))
            {
                var producer = container.Resolve<IFormulaProducer>();

                producer.AddFormula("a=1").Wait();

                Assert.AreEqual(1.0, producer.GetVariable("a").Result.VariableValue);
            }
        }

        /// <summary>
        ///     Проверяет корректность обработки формулы "a= 10 + ** 5"
        /// </summary>
        [TestMethod]
        public void Send_A_EqualTo_Incorrect_Expression()
        {
            using (var container = this.CreateContainer(this.InjectMockRepositary<FormulaRepositary, FormulaEntity>))
            {
                var producer = container.Resolve<IFormulaProducer>();

                var ex = Assert.ThrowsException<AggregateException>(() => producer.AddFormula("a= 10 + ** 5").Wait());

                Assert.IsInstanceOfType(ex.Flatten().InnerExceptions[0], typeof(SmsCodeCompileException));
                var a = producer.GetVariable("a").Result;

                Assert.IsNull(a);
            }
        }

        /// <summary>
        ///     Проверяет корректность обработки формулы "a = a * 9"
        /// </summary>
        [TestMethod]
        public void Send_A_DependentOf_A()
        {
            using (var container = this.CreateContainer(this.InjectMockRepositary<FormulaRepositary, FormulaEntity>))
            {
                var producer = container.Resolve<IFormulaProducer>();

                producer.AddFormula("a = a * 9").Wait();

                var a = producer.GetVariable("a").Result;
                Assert.AreEqual(FormulaStatus.Failed, a.Status);
                Assert.IsTrue(a.ErrorDescription.Contains("Circular reference detected for variable"));
            }
        }

        /// <summary>
        ///     Проверяет корректность обработки формул: "a = b + 10", "b = 9 * 10"
        /// </summary>
        [TestMethod]
        public void Send_A_DependentOf_B_Then_Send_B()
        {
            using (var container = this.CreateContainer(this.InjectMockRepositary<FormulaRepositary, FormulaEntity>))
            {
                var producer = container.Resolve<IFormulaProducer>();

                Debug.WriteLine($"Send_A_DependentOf_B_Then_Send_B,.ThreadId: #{Thread.CurrentThread.ManagedThreadId}");

                producer.AddFormula("a = b + 10").Wait();

                Assert.ThrowsException<AggregateException>(() => producer.GetVariable("a", 5000).Result);

                var a = producer.GetVariable("a", waitTermination: false).Result;

                Assert.AreEqual(FormulaStatus.WaitingForDependency, a.Status);
                Assert.IsNull(a.VariableValue);

                var b = producer.GetVariable("b").Result;

                Assert.IsNull(b);

                producer.AddFormula("b = 9 * 10").Wait();

                a = producer.GetVariable("a", 5000).Result;

                Assert.AreEqual(FormulaStatus.Calculated, a.Status);
                Assert.AreEqual(100, a.VariableValue);

                b = producer.GetVariable("b", 100).Result;

                Assert.AreEqual(FormulaStatus.Calculated, b.Status);
                Assert.AreEqual(90, b.VariableValue);
            }
        }

        /// <summary>
        ///     Проверяет корректность обработки формул: "a = b + c", "b = c + d", "c = a + 10"
        /// </summary>
        [TestMethod]
        public void Send_A_DependentOf_B_Then_B_DependentOf_C_Then_C_Dependent_Of_A()
        {
            using (var container = this.CreateContainer(this.InjectMockRepositary<FormulaRepositary, FormulaEntity>))
            {
                var producer = container.Resolve<IFormulaProducer>();

                producer.AddFormula("a = b + c").Wait();

                Assert.ThrowsException<AggregateException>(() => producer.GetVariable("a", 5000).Result);

                var a = producer.GetVariable("a", waitTermination: false).Result;

                Assert.AreEqual(FormulaStatus.WaitingForDependency, a.Status);
                Assert.IsNull(a.VariableValue);

                producer.AddFormula("b = c + d").Wait();

                Assert.ThrowsException<AggregateException>(() => producer.GetVariable("b", 5000).Result);

                var b = producer.GetVariable("b", waitTermination: false).Result;
                Assert.AreEqual(FormulaStatus.WaitingForDependency, b.Status);
                Assert.IsNull(b.VariableValue);

                a = producer.GetVariable("a", waitTermination: false).Result;
                Assert.AreEqual(FormulaStatus.WaitingForDependency, a.Status);
                Assert.IsNull(a.VariableValue);

                producer.AddFormula("c = a + 10").Wait();

                var c = producer.GetVariable("c").Result;
                Assert.AreEqual(FormulaStatus.Failed, c.Status);
                Assert.IsNull(c.VariableValue);
                Assert.IsTrue(c.ErrorDescription.Contains("Circular reference detected for variable"));

                b = producer.GetVariable("b", waitTermination: false).Result;
                Assert.AreEqual(FormulaStatus.Failed, b.Status);
                Assert.IsNull(b.VariableValue);
                Assert.IsTrue(b.ErrorDescription.Contains("Circular reference detected for variable"));

                a = producer.GetVariable("a", waitTermination: false).Result;
                Assert.AreEqual(FormulaStatus.Failed, a.Status);
                Assert.IsNull(a.VariableValue);
                Assert.IsTrue(a.ErrorDescription.Contains("Circular reference detected for variable"));
            }
        }

        /// <summary>
        ///     Проверяет корректность обработки формул: "A = 12/3", "C = (A - B) * 4.2 + 23", "B = 0.3"
        /// </summary>
        [TestMethod]
        public void SimpleTest()
        {
            using (var container = this.CreateContainer(this.InjectMockRepositary<FormulaRepositary, FormulaEntity>))
            {
                var producer = container.Resolve<IFormulaProducer>();

                producer.AddFormula("A = 12/3").Wait();
                producer.AddFormula("C = (A - B) * 4.2 + 23").Wait();
                producer.AddFormula("B = 0.3").Wait();

                var a = producer.GetVariable("A").Result;
                Assert.AreEqual(FormulaStatus.Calculated, a.Status);
                Assert.AreEqual(4, a.VariableValue);

                var b = producer.GetVariable("B").Result;
                Assert.AreEqual(FormulaStatus.Calculated, b.Status);
                Assert.AreEqual(0.3, b.VariableValue);

                var c = producer.GetVariable("C").Result;
                Assert.AreEqual(FormulaStatus.Calculated, c.Status);
                Assert.AreEqual(38.54, c.VariableValue);
            }
        }
    }
}