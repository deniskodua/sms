# Сервис расчета формул

Сервис расчета формул состоит из клиентской и серверной частей.

- Серверная часть - проект Sms.FormulaServer.

- Клиентская часть - Sms.FormulaClient

## Sms.FormulaServer

Проект представляет из себя консольное приложение, в котором запущен AutoHosted WCF сервер.

В App.config требуется указать строку подключения к БД MS SQL Server.
```xml
<connectionStrings>
    <add name="CodeDb" connectionString="data source=.;initial catalog=SmsCodeDb;Integrated Security=True;App=SmsCodeCalculator" providerName="System.Data.SqlClient" />
</connectionStrings>
```

При необходимости, можно настроить параметры WCF в app.config.
По умолчанию, сервис работает на порту 5000:

```xml
        <services>
            <service name="Sms.FormulaServer.FormulaService">
                <endpoint address="" binding="basicHttpBinding" contract="Sms.FormulaServer.IFormulaService">
                    <identity>
                        <dns value="localhost" />
                    </identity>
                </endpoint>
                <endpoint address="mex" binding="mexHttpBinding" contract="IMetadataExchange" />
                <host>
                    <baseAddresses>
                        <add baseAddress="http://localhost:5000/" />
                    </baseAddresses>
                </host>
            </service>
        </services>
```

Для запуска сервера просто вызываем **Sms.FormulaServer.exe**
Если все впорядке, то появится сообщение

![Сервер готов к работе](./Images/ServerReady.png)

---

## Sms.FormulaClient

Проект является консольным приложением. 
Перед запуском необходимо настроить App.config
По умолчанию, клиент обращается к серверу по адресу http://localhost:5000.

```xml
<system.serviceModel>
        <bindings>
            <basicHttpBinding>
                <binding name="BasicHttpBinding_IFormulaService" />
            </basicHttpBinding>
        </bindings>
        <client>
            <endpoint address="http://localhost:5000/" binding="basicHttpBinding"
                bindingConfiguration="BasicHttpBinding_IFormulaService" contract="Client.IFormulaService"
                name="BasicHttpBinding_IFormulaService" />
        </client>
    </system.serviceModel>
```

После запуска приложения появится консоль команд Sms.FormulaClient:

![Сервер готов к работе](./Images/ClientTerminalEmpty.png)

Поддерживается 3 вида команд:

1) **-f**  - команда добавления новой формулы

2) **-v**  команда получения значения переменной

3) **-q** выход из приложения.

#### Примеры использования:

![Сервер готов к работе](./Images/ClientExample.png)
